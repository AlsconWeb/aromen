<?php
function my_logged_in_redirect() {
     
    if ( !current_user_can('administrator') ) 
    {
        wp_redirect( 'https://aromen-wellness.com' );
        die;
    }
     
}
//add_action( 'template_redirect', 'my_logged_in_redirect' );



function aromen_enqueue_styles() {

    $parent_style = 'organic-beauty'; 
	
	wp_enqueue_style( 'google-fonts', "https://fonts.googleapis.com/css?family=Montserrat" );
	wp_enqueue_style( 'abril-fonts', "https://use.typekit.net/lga0mqp.css" );	
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );

    // enqueue scripts
   
    wp_enqueue_script( 'font-awesome', 'https://use.fontawesome.com/b81aa3fbe0.js', array( 'jquery' ), '1.0.0', true );
    wp_enqueue_script( 'theme-scripts', get_stylesheet_directory_uri() . '/includes/js/scripts.js', array( 'jquery' ), '1.0.0', true );
	

}
add_action( 'wp_enqueue_scripts', 'aromen_enqueue_styles' );


function my_admin_scripts() {
    
}
add_action( 'admin_enqueue_scripts', 'my_admin_scripts' );


require_once('includes/custom-functions.php');

/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

// add_filter( 'style_loader_src',  'sdt_remove_ver_css_js', 9999, 2 );
// add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999, 2 );

function sdt_remove_ver_css_js( $src, $handle ) 
{
    $handles_with_version = [ 'style' ]; // <-- Adjust to your needs!

    if ( strpos( $src, 'ver=' ) && ! in_array( $handle, $handles_with_version, true ) )
        $src = remove_query_arg( 'ver', $src );

    return $src;
}

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 24;
  return $cols;
}


add_action( 'woocommerce_email_before_order_table', function(){
    if ( ! class_exists( 'WC_Payment_Gateways' ) ) return;

    $gateways = WC_Payment_Gateways::instance(); // gateway instance
    $available_gateways = $gateways->get_available_payment_gateways();

    if ( isset( $available_gateways['bacs'] ) )
        remove_action( 'woocommerce_email_before_order_table', array( $available_gateways['bacs'], 'email_instructions' ), 10, 3 );
}, 1 );

/**
 * Adds the ability to sort products in the shop based on the SKU
 * Can be combined with tips here to display the SKU on the shop page: https://www.skyverge.com/blog/add-information-to-woocommerce-shop-page/
 */

function sv_add_sku_sorting( $args ) {

	$orderby_value = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );

	if ( 'sku' == $orderby_value ) {
		$args['orderby'] = 'meta_value';
    		$args['order'] = 'asc'; 
    		// ^ lists SKUs alphabetically 0-9, a-z; change to desc for reverse alphabetical
		$args['meta_key'] = '_sku';
	}

	return $args;
}
add_filter( 'woocommerce_get_catalog_ordering_args', 'sv_add_sku_sorting' );


function sv_sku_sorting_orderby( $sortby ) {
	$sortby['sku'] = 'Sort by SKU';
	// Change text above as desired; this shows in the sorting dropdown
	return $sortby;
}
add_filter( 'woocommerce_catalog_orderby', 'sv_sku_sorting_orderby' );
add_filter( 'woocommerce_default_catalog_orderby_options', 'sv_sku_sorting_orderby' );

add_action( 'init', 'prefix_remove_bank_details', 100 );
function prefix_remove_bank_details() {
	
	// Do nothing, if WC_Payment_Gateways does not exist
	if ( ! class_exists( 'WC_Payment_Gateways' ) ) {
		return;
	}

	// Get the gateways instance
	$gateways           = WC_Payment_Gateways::instance();
	
	// Get all available gateways, [id] => Object
	$available_gateways = $gateways->get_available_payment_gateways();

	if ( isset( $available_gateways['bacs'] ) ) {
		// If the gateway is available, remove the action hook
		remove_action( 'woocommerce_email_before_order_table', array( $available_gateways['bacs'], 'email_instructions' ), 10, 3 );
	}
}

function ml_discount_info() {
	echo '<div class="ml-discount-info"><span>From 10 bottles of 10ml: <strong>-40%</strong></span><span>From 3 bottles of 100ml: <strong>-8%</strong></span></div>';
	
}
// add_action('woocommerce_before_add_to_cart_form','ml_discount_info');

function ml_disable_register(){
	if(is_page('my-account') && !is_user_logged_in()){
		wp_redirect( '/' );
		exit;
	}
}
//add_action('wp_head', 'ml_disable_register');


add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     $fields['shipping']['shipping_phone'] = array(
        'label'     => __('Phone', 'woocommerce'),
    'placeholder'   => _x('Phone', 'placeholder', 'woocommerce'),
    'required'  => true,
    'class'     => array('form-row-wide'),
    'clear'     => true
	 );
	 $fields['shipping']['shipping_email'] = array(
        'label'     => __('Email', 'woocommerce'),
    'placeholder'   => _x('Email', 'placeholder', 'woocommerce'),
    'required'  => true,
    'class'     => array('form-row-wide'),
    'clear'     => true
	 );
	 unset($fields['shipping']['shipping_postcode']['validate']);

     return $fields;
}


/*
 * Display field value on the order edit page
 */
 
add_action( 'woocommerce_admin_order_data_after_shipping_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
	echo '<p><strong>'.__('Phone: ').':</strong> ' . get_post_meta( $order->get_id(), '_shipping_phone', true ) . '</p>';
	echo '<p><strong>'.__('Email: ').':</strong> ' . get_post_meta( $order->get_id(), '_shipping_email', true ) . '</p>';
}


// add_action('wpml_language_has_switched', 'ml_qo_logout'); 

// function ml_qo_logout(){
// 	if(is_user_logged_in()){
// 		wp_logout();
// 		wp_redirect( '/' );
// exit;
// 	}
// }

add_filter( 'wc_product_enable_dimensions_display', '__return_false' );


if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}
add_action('woocommerce_archive_description', 'woo_cat_featured_image',1);

function woo_cat_featured_image(){ ?>    
    <?php 

        
        $term_id = get_queried_object()->term_id;	 
        $post_id = 'term_'.$term_id;
        //echo $post_id;
    
    ?>

    <?php if(get_field('featured_image', $post_id)) { ?>
        <div class="cat-featured-image"><img class="cat-featured-img" src="<?php echo get_field('featured_image', $post_id);?>"/></div>
    <?php } ?>
    
    <h1 class="page_title text-left"><?php echo strip_tags(organic_beauty_get_blog_title()); ?></h1>

<?php
}

add_filter( 'action_scheduler_retention_period', 'wpb_action_scheduler_purge' );
/**
 * Change Action Scheduler default purge to 1 week
 */
function wpb_action_scheduler_purge() {
 return WEEK_IN_SECONDS;
}

// Custom badges //

add_filter('woocommerce_sale_flash', 'lw_hide_sale_flash');
    function lw_hide_sale_flash()
    {
        return false;
    }
$badges = get_field('badges','option');

if($badges['badge_new']['onoff']) {

add_action('woocommerce_before_single_product_summary','mk_custom_badge_single',15);

    function mk_custom_badge_single() {
        global $product;
        global $badges;
        echo '<div class="badge_container_single">' ;
        
        if ( $product->get_total_sales() > $badges['badge_new']['bestseller_total_sales']) {
            
            echo '<span class="badge_single bestseller"  >' . esc_html__( 'BESTSELLER', 'woocommerce' ) . '</span>';
        } 
        
        if ( !$product->is_in_stock() ) { 
        echo '<span class="badge_single soldout" >' . esc_html__( 'SOLD OUT', 'woocommerce' ) . '</span>';
        }
        
        if ( $product->is_on_sale() ) {
            echo '<span class="badge_single sell_out" >' . esc_html__( 'SALE', 'woocommerce' ) . '</span>';
        } 
        
        $created = strtotime( $product->get_date_created() );
        if ( ( time() - ( 60 * 60 * 24 * $badges['badge_new']['days'] ) ) < $created ) {
        echo '<span class="badge_single" >' . esc_html__( 'NEW', 'woocommerce' ) . '</span>';
        }
        echo '</div>';	

        }
    }

add_action('woocommerce_product_thumbnails','mk_eco_badge_single',10);
    function mk_eco_badge_single() {
        global $product;
        $sku = $product->get_sku(); 
        
            echo '<div style="position:relative; height:0;">'; 
                echo '<div class="badge_eco_container_single">';
                    if( has_term( 'fairtrade', 'product_tag')) {
                        echo '<span class="badge_eco_single"> <img src=' . get_stylesheet_directory_uri() .'/assets/img/fairtrade-badge.svg> </span>';
                    }
                    if( has_term( 'bio', 'product_tag') || strpos($sku,'BIO')) {
                        echo '<span class="badge_eco_single"> <img src=' . get_stylesheet_directory_uri() .'/assets/img/bio-badge.svg> </span>'; 
                    }
                echo '</div>';
            echo '</div>';
    }

if($badges['badge_new']['onoff']) {
	
	add_action('woocommerce_before_shop_loop_item','mk_custom_badge',10);

        function mk_custom_badge() {
            global $product;
            global $badges;

            echo '<div style="position:relative; height:0;">';
            echo '<div class="badge_container">' ;
            
            if ( $product->get_total_sales() > $badges['badge_new']['bestseller_total_sales']) {
                echo '<span class="badge bestseller besseller_category"" >' . esc_html__( 'BESTSELLER', 'woocommerce' ) . '</span>';
            } 
            
            if ( !$product->is_in_stock() ) { 
            echo '<span class="badge soldout soldout_category" >' . esc_html__( 'SOLD OUT', 'woocommerce' ) . '</span>';
            }
            
            if ( $product->is_on_sale() ) {
                echo '<span class="badge sell_out sell_out_category" >' . esc_html__( 'SALE', 'woocommerce' ) . '</span>';
            } 
            
            $created = strtotime( $product->get_date_created() );
            if ( ( time() - ( 60 * 60 * 24 * $badges['badge_new']['days'] ) ) < $created ) {
            echo '<span class="badge new_category">' . esc_html__( 'NEW', 'woocommerce' ) . '</span>';
            }
            echo '</div>';	
            echo '</div>';
        }
}

add_action( 'woocommerce_before_single_product', 'customise_product_page' );
    function customise_product_page() {
        remove_action('woocommerce_before_subcategory_title','organic_beauty_woocommerce_open_item_wrapper',20);
	    remove_action('woocommerce_before_shop_loop_item_title','organic_beauty_woocommerce_open_item_wrapper',20);

	    add_action('woocommerce_before_subcategory_title','organic_beauty_woocommerce_open_item_wrapper_new',20);
	    add_action('woocommerce_before_shop_loop_item_title','organic_beauty_woocommerce_open_item_wrapper_new',20);
    }

    function organic_beauty_woocommerce_open_item_wrapper_new($cat='') {
		?>
				</a>
			<?php 
            do_action( 'custom_eco_badge_hook' ); 
            ?>
			</div>
			
		</div>

		<div class="post_content"> 
		<?php
	}

add_action('woocommerce_before_shop_loop_item_title','mk_eco_badge',10);
add_action('custom_eco_badge_hook','mk_eco_badge',10);

    function mk_eco_badge() {
        global $product;
        $sku = $product->get_sku(); 

        echo '<div style="position:relative; height:0;">'; 
            echo '<div class="badge_eco_container">';
                if( has_term( 'fairtrade', 'product_tag') ) {
                       echo '<span class="badge_eco"> <img src=' . get_stylesheet_directory_uri() .'/assets/img/fairtrade-badge.svg> </span>';
                }
                if( has_term( 'bio', 'product_tag') || strpos($sku,'BIO')) {
                       echo '<span class="badge_eco"> <img src=' . get_stylesheet_directory_uri() .'/assets/img/bio-badge.svg> </span>';                            
                }
            echo '</div>';
        echo '</div>';
    }

// Essential grid badges
add_shortcode( 'selling_status', 'display_product_stock_status' );
    function display_product_stock_status( $atts) {
        global $badges;
        $atts = shortcode_atts(
            array('id'  => get_the_ID() ),
            $atts, 'selling_status'
        );
    
        if( intval( $atts['id'] ) > 0 && function_exists( 'wc_get_product' ) ){
            $product = wc_get_product( $atts['id'] );
        
            echo '<div style="position:relative; height:0;">';
            echo '<div class="essential-badge_container">' ;
            
            if ( $product->get_total_sales() > $badges['badge_new']['bestseller_total_sales']) {
                echo '<span class="essential_badge essential_bestseller bestseller" >' . esc_html__( 'BESTSELLER', 'woocommerce' ) . '</span>';
            } 
            
            if ( !$product->is_in_stock() ) { 
                echo '<span class="essential_badge soldout" >' . esc_html__( 'SOLD OUT', 'woocommerce' ) . '</span>';
            }
            
            if ( $product->is_on_sale() ) {
                echo '<span class="essential_badge essential_sell_out sell_out" >' . esc_html__( 'SALE', 'woocommerce' ) . '</span>';
            } 
            
            $created = strtotime( $product->get_date_created() );
            if ( ( time() - ( 60 * 60 * 24 * $badges['badge_new']['days'] ) ) < $created ) {
                echo '<span class="essential_badge">' . esc_html__( 'NEW', 'woocommerce' ) . '</span>';
            }
            echo '</div>';	
            echo '</div>';
        }
    }

add_shortcode( 'bio_status', 'display_bio_status' );
    function display_bio_status( $atts) {
        $atts = shortcode_atts(
            array('id'  => get_the_ID() ),
            $atts, 'bio_status'
        );
    
        if( intval( $atts['id'] ) > 0 && function_exists( 'wc_get_product' ) ){
            $product = wc_get_product( $atts['id'] );
        
            $sku = $product->get_sku(); 

            echo '<div style="height:0;">'; 
                echo '<div class="essential_badge_eco_container">';
                    if( has_term( 'fairtrade', 'product_tag', $post = $atts['id']) ) {
                        echo '<span class="essential_badge_eco"> <img src=' . get_stylesheet_directory_uri() .'/assets/img/fairtrade-badge.svg> </span>';
                    }
                    if( has_term( 'bio', 'product_tag', $post = $atts['id']) || strpos($sku,'BIO')) {
                        echo '<span class="essential_badge_eco"> <img src=' . get_stylesheet_directory_uri() .'/assets/img/bio-badge.svg> </span>';                            
                    }
                echo '</div>';
            echo '</div>';
        }
    }


add_action( 'widgets_init', 'my_custom_sidebar');
    
    function my_custom_sidebar() {
        register_sidebar(
            array (
                'name' => __( 'Custom', 'one' ),
                'id' => 'custom-side-bar',
                'description' => __( 'Custom Sidebar', 'one' ),
                'before_widget' => '<div class="widget-content">',
                'after_widget' => "</div>",
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>',
            )
        );
    }
    
add_action( 'woocommerce_before_main_content', 'test', 10);

    function test() {
        if (is_product_category()) {
            
            return get_sidebar('my-custom'); 
        }
    }



add_action( 'after_setup_theme', 'my_remove_product_result_count', 99 );
    function my_remove_product_result_count() { 
        remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );
        remove_action( 'woocommerce_after_shop_loop' , 'woocommerce_result_count', 20 );
    }

remove_action('woocommerce_before_main_content','woocommerce_breadcrumb',20);
add_action('woocommerce_before_shop_loop','woocommerce_breadcrumb',20);


remove_filter('organic_beauty_filter_get_blog_title', 'organic_beauty_woocommerce_get_blog_title', 9, 2);
add_filter('organic_beauty_filter_get_blog_title', 'organic_beauty_woocommerce_get_blog_title_new', 9, 2);
    
    function organic_beauty_woocommerce_get_blog_title_new($title, $page) {
				

		if (!empty($title)) return $title;
		
		if ( organic_beauty_strpos($page, 'woocommerce')!==false ) {
			if ( $page == 'woocommerce_category' ) {
				$term = get_term_by( 'slug', get_query_var( 'product_cat' ), 'product_cat', OBJECT);
				$title = $term->name;
			} else if ( $page == 'woocommerce_tag' ) {
				if(is_product_category()) {
					$term = get_term_by( 'slug', get_query_var( 'product_cat' ), 'product_cat', OBJECT);
					$title = $term->name;

				} else {
									$term = get_term_by( 'slug', get_query_var( 'product_tag' ), 'product_tag', OBJECT);
				$title = esc_html__('Tag:', 'organic-beauty') . ' ' . esc_html($term->name);
				$title = esc_html__($title);
				}
			
			} else if ( $page == 'woocommerce_cart' ) {
				$title = esc_html__( 'Your cart', 'organic-beauty' );
			} else if ( $page == 'woocommerce_checkout' ) {
				$title = esc_html__( 'Checkout', 'organic-beauty' );
			} else if ( $page == 'woocommerce_account' ) {
				$title = esc_html__( 'Account', 'organic-beauty' );
			} else if ( $page == 'woocommerce_product' ) {
				$title = organic_beauty_get_post_title();
			} else if (($page_id=get_option('woocommerce_shop_page_id')) > 0) {
				$title = organic_beauty_get_post_title($page_id);
			} else {
				$title = esc_html__( 'Shop', 'organic-beauty' );
			}
		}
		
		return $title;
	}