jQuery(document).ready(function( $ ) {

	
	
	//check if is variation product
	
	let isVariablewithBatch = () => {

		if($('body').hasClass('single-product') && $('.summary table').hasClass('variations') && $('#pa_batch-number').length > 0){
			return true;
		}

	}
	

	let ml_ajax_addToCart = (ajaxProducts) => {
		
	    
	    let data = {
	        'action': 'aromen_add_to_cart',
	        'product_id': ajaxProducts['product_id'],
	        'product_qty': ajaxProducts['qty'],
	        'product_size': ajaxProducts['size']
	        
	      };

	    jQuery.post(
			wc_add_to_cart_params.ajax_url, 
			data, 
			function(response){
				
				if ( ! response )
				return;	
				console.log(response);
				
				let this_page = window.location.toString();
	      		this_page = this_page.replace( 'add-to-cart', 'added-to-cart' );		

	      		if ( response.error && response.product_url ) {
		      		window.location = response.product_url;
		      		return;
	      		}	

	      		if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
			  		window.location = wc_add_to_cart_params.cart_url;
			  		return;
	            } else {

	            	var $fragment_refresh = {
	            			url: wc_cart_fragments_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'get_refreshed_fragments' ),
	            			type: 'POST',
	            			success: function( data ) {


	            				if ( data && data.fragments ) {

	            					// $.each( data.fragments, function( key, value ) {
	            					// 	$( key ).replaceWith( value );
	            					// });								
	            						
	            					
	            					
	            					$('.widget_shopping_cart_content').empty().html(data.fragments['div.widget_shopping_cart_content']);
	            					$('span.ess-cart-content').empty().html(data.fragments['span.ess-cart-content']);
	            					$('.contact_cart_totals').detach();
	            					$('.top_panel_cart_button').append(data.fragments['.contact_cart_totals']);
	            					//$('.woocommerce-variation-availability p').empty().html(response.fragments['.woocommerce-error']);
	            					//$('.woocommerce-breadcrumb').after(response.fragments['.woocommerce-error']);
	            					


	            					$( document.body ).trigger( 'wc_fragments_refreshed' );
	            					$("html, body").animate({ scrollTop: 0 }, "slow");
	            					
	            					//show cart 
	            					if($('.widget_area.sidebar_cart.sidebar').css('display') != 'block'){
	            						jQuery('a.top_panel_cart_button').click();
	            					}

	            				}
	            			}
	            		};
	            	

	            	$.ajax( $fragment_refresh );


	            }

				

	      		/*
	      		let this_page = window.location.toString();
	      		this_page = this_page.replace( 'add-to-cart', 'added-to-cart' );

	      		if ( response.error && response.product_url ) {
		      		window.location = response.product_url;
		      		return;
	      		}*/
	      // 		if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
			  		// window.location = wc_add_to_cart_params.cart_url;
			  		// return;
	      //       } else {
	      //       	//$thisbutton.removeClass( 'loading' );
	      //       	let fragments = response.fragments;
	      //       	console.log(response.fragments);
	      //       	let cart_hash = response.cart_hash;
	      //       	if ( fragments ) {
		     //        	$.each( fragments, function( key ) {
			    //         	$( key ).addClass( 'updating' );
		     //        	});
	      //       	}
	      //       }    
	          
	          
	      	}
	  	);	  	
	  
	}

	if(isVariablewithBatch()) {

		let variable_product = [];
		let quantity_field = $('.input-text.qty.text');
		let size_field = $('#pa_size');

		//hide batch number
		$('#pa_batch-number').parent().parent().hide();
		
		// $( document ).on( 'click', '.single_add_to_cart_button', function(e) {

		// 	e.preventDefault();
		// 	console.log('add_to_cart clicked');
		// 	variable_product['product_id'] 	= jQuery('.variations_form').data('product_id');
		// 	variable_product['qty'] 		= quantity_field.val();
		// 	variable_product['size'] 		= size_field.val();

		// 	if ( typeof wc_add_to_cart_params === 'undefined' ) {
		// 		return false;
		// 	} else {
				
				
		// 		ml_ajax_addToCart(variable_product);

		// 	}

		// });

	}

	jQuery( '.shop_table.cart' ).closest( 'form' ).find( '[name="update_cart"]' ).removeProp( 'disabled');

	let treesCount = {
		treesNumber : '',

		events: function(){
			var self = this;
			$( ".input-text.qty" ).change(function() {				
				console.log($(this).val());
				var newTreesNumber = self.treesNumber * parseInt($(this).val());
				$('.trees-number .num').text('+ ' + newTreesNumber);


			  });
			$(document.body).on('click','div.quantity', function(){
				console.log('clicked');
				// var newTreesNumber = $( ".input-text.qty" ).val() * self.treesNumber;
				// console.log(newTreesNumber);

			});
			
			jQuery('.woocommerce div.quantity,.woocommerce-page div.quantity').on('click', '>span', function(e) {
                
				e.preventDefault();
				var f = jQuery(this).siblings('input');
				if (jQuery(this).hasClass('q_inc')) {
					
					var newTreesNumber = ( parseInt($( ".input-text.qty" ).val()) + 1) * self.treesNumber;
					$('.trees-number .num').text('+ ' + newTreesNumber);

				} else {
					if(parseInt($( ".input-text.qty" ).val()) > 1) {
						var newTreesNumber = (parseInt($( ".input-text.qty" ).val()) - 1) * self.treesNumber;
						$('.trees-number .num').text('+ ' + newTreesNumber);
					}
					
				}
               
            });
			
		}, 

		init: function() {
			var self = this;
			console.log('treesCount');
			self.treesNumber = parseInt($('.trees-number .num').text().replace('+ ', ''));
			var newTreesNumber = (parseInt($( ".input-text.qty" ).val())) * self.treesNumber;
		
			$('.trees-number .num').text('+ ' + newTreesNumber);
			this.events();
			
			
		}
		
	}
	if($('body').hasClass('single-product')){
		treesCount.init();

	}

	
});