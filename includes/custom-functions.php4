<?php 

function compare_by_batch($a, $b) {
  return strcmp($a['attributes']["attribute_pa_batch-number"], $b['attributes']["attribute_pa_batch-number"]);
}

function select_Variations($variations, $attribute, $value){

	$new_Array = [];

	foreach ($variations as $variation){

		if($variation['attributes'][$attribute] == $value){
			
			$new_Array[] = $variation;

		}

	}

	// sort variations by batch number
	usort($new_Array, 'compare_by_batch');

	return $new_Array;
}


function get_batch($sku, $size, $qty){

	$batch = '';
	global $wpdb;
	$table_name = $wpdb->prefix . "excel_stock";
	$results = $wpdb->get_results( "SELECT * FROM ". $wpdb->prefix ."excel_stock");

	foreach ($results as $result) {
		if($result->sku == $sku && strtolower(str_replace(" ", "",$result->size)) == strtolower(str_replace(" ", "",$size)) && intval($result->available_stock) >= intval($qty)){

			return $result->batch;

		}
	}

	return $batch ;

}
function select_batch_variation($variations, $size, $batch){

	$new_Array = [];

	foreach ($variations as $variation){

		if(strtolower($variation['attributes']['attribute_pa_batch-number']) == strtolower($batch) && strtolower($variation['attributes']['attribute_pa_size']) == strtolower($size)){
			
			$new_Array[] = $variation;

		}

	}

	// sort variations by batch number
	//usort($new_Array, 'compare_by_batch');

	return $new_Array;
}




function aromen_add_to_cart(){

	ob_start();
	
	$products['product_id'] = stripslashes($_POST['product_id']);
	$products['qty'] = stripslashes($_POST['product_qty']);
	$products['size'] = stripslashes($_POST['product_size']);
	
	
	
	// global $product;
	
	// if ( $product->is_type( 'variable' ) ) {


		$product_variable = new WC_Product_Variable($products['product_id']);
		$product_variations = $product_variable->get_available_variations();
		$product_sku = $product_variable->get_sku();
		$product_batch = get_batch($product_sku, $products['size'], $products['qty']);
		// select variation with selected size 
		//$selected_variations = select_Variations($product_variations, 'attribute_pa_size', $products['size']);
		$selected_variations = select_batch_variation($product_variations, $products['size'], $product_batch);
		$count_selected_variations = count($selected_variations);

		

		// check if there is enough products with one batch number
		// find one with enough quantity
		$find_index = -1;
		for ($i = 0; $i < $count_selected_variations; $i++) {

			if($selected_variations[$find_index]['attributes']['attribute_pa_batch-number'] != 'default' && $products['qty'] <= $selected_variations[$i]['max_qty']){

				$find_index = $i;

			}

		}

		$find_index = 0;

 // $data = array(
 // 		'yy' => $count_selected_variations

 // );
	// 	  wp_send_json( $data );
		//if there is enough


		if($products['qty'] <= $selected_variations[$find_index]['max_qty']){
			ob_start();
			// add to cart

			$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $products['product_id'], $products['qty'], $selected_variations[$find_index]['variation_id'], (array) $selected_variations[$find_index]['attributes'], $cart_item_data );

			if ( $passed_validation && WC()->cart->add_to_cart( $products['product_id'], $products['qty'], $selected_variations[$find_index]['variation_id'], (array) $selected_variations[$find_index]['attributes'] )){

				do_action( 'woocommerce_ajax_added_to_cart', $products['product_id'] );

				if ( get_option( 'woocommerce_cart_redirect_after_add' ) == 'yes' ) {
		            wc_add_to_cart_message( $products['product_id'] );

		        }

				WC_AJAX::get_refreshed_fragments();
				
				
			} else {

		        // If there was an error adding to the cart, redirect to the product page to show any errors
		        $data = array(
		            'error' => true,
		            'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $products['product_id'] ), $products['product_id'] )
		        );

		        wp_send_json( $data );

		    } 


		} 
		
		
	die();
}

add_action('wp_ajax_aromen_add_to_cart', 'aromen_add_to_cart');
add_action('wp_ajax_nopriv_aromen_add_to_cart', 'aromen_add_to_cart');



function woocommerce_header_add_to_cart_fragment( $fragments ) { 
	global $woocommerce; 

	$cart_items = WC()->cart->get_cart_contents_count();
	$cart_summa = strip_tags(WC()->cart->get_cart_subtotal());

	ob_start(); ?>

	<span class="contact_cart_totals">
		<span class="cart_items"><?php
			echo esc_html($cart_items) . ' ' . ($cart_items == 1 ? esc_html__('Item', 'organic-beauty') : esc_html__('Items', 'organic-beauty'));
		?></span>
		- 
		<span class="cart_summa"><?php organic_beauty_show_layout($cart_summa); ?></span>
	</span>

	<?php 
	
	$fragments['.contact_cart_totals'] = ob_get_clean(); 	
		
	 return $fragments; 
	 die();

 }// end ajax cart update

add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment'); 



// create table for storing data from excel sheet
function create_excel_stock_table(){

	global $wpdb;
	$table_name = $wpdb->prefix.'excel_stock';
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
	     //table not in database. Create new table
	     $charset_collate = $wpdb->get_charset_collate();
	 
	     $sql = "CREATE TABLE $table_name (
	          id mediumint(9) NOT NULL AUTO_INCREMENT,
	          sku text NOT NULL,	          
	          size text NOT NULL,	          
	          stock text NOT NULL,	          
	          batch text NOT NULL,	          
	          available_stock text NOT NULL,	          
	          is_added text NOT NULL,	          
	          UNIQUE KEY id (id)
	     ) $charset_collate;";
	     require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	     dbDelta( $sql );

	     //register the new table with the wpdb object
         if (!isset($wpdb->excel_stock))
         {
             $wpdb->excel_stock = $table_name;
             //add the shortcut so you can use $wpdb->stats
             $wpdb->tables[] = str_replace($wpdb->prefix, '', $table_name);
         }

	}


}
add_action( 'init', 'create_excel_stock_table');


// add table view to admin dashboard
	
add_action('admin_menu', 'add_excel_stock_page');
function add_excel_stock_page() {

add_menu_page( 'Excel Stock', 'Excel Stock', 'manage_options', 'excel-stock', 'excel_stock_page', $icon_url = '', $position = "6" );

}
function excel_stock_page(){

?>
<?php  update_excel_sheet_table(); ?>
<div>
<h2>Stocks from Excel</h2>

<?php 

	global $wpdb;
	$table_name = "excel_stock";
	$results = $wpdb->get_results( "SELECT * FROM ". $wpdb->prefix ."excel_stock");

	
	echo '<table class="widefat fixed" cellspacing="0">';

	echo '<thead><tr>
				<th id="ID" class="manage-column column-columnname num" scope="col">ID</th>
				<th id="SKU" class="manage-column column-columnname" scope="col">SKU</th>
				<th id="Size" class="manage-column column-columnname" scope="col">Size</th>
				<th id="Stock" class="manage-column column-columnname" scope="col">Stock</th>
				<th id="Batch" class="manage-column column-columnname" scope="col">Batch</th>
				<th id="Available" class="manage-column column-columnname" scope="col">Available</th>
				<th id="Is_Added" class="manage-column column-columnname" scope="col">Is Added</th>
					
			</tr></thead><tbody>';
	foreach ( $results as $result){

		?>

		<tr>
			<td><?php echo $result->id; ?></td>
			<td><?php echo $result->sku; ?></td>
			<td><?php echo $result->size; ?></td>
			<td><?php echo $result->stock; ?></td>
			<td><?php echo $result->batch; ?></td>
			<td><?php echo $result->available_stock; ?></td>
			<td><?php echo $result->is_added; ?></td>
		
		</tr>


		<?php
		
	}
 ?>
</tbody></table>
<?php add_to_stock(); ?>
</div>
 
<?php
}


// get excel sheet data

function get_excel_sheet(){
	$api_key = 'AIzaSyDbRhm81bjmB1MVDOgha8CjxeAIzq-_LXw';
	$sheet_id = '1cikiYnFJp2e-S-m9GVNG_BHLSAulHCeCO_qlWsncNhI';
	$url = 'https://docs.google.com/spreadsheets/d/1cikiYnFJp2e-S-m9GVNG_BHLSAulHCeCO_qlWsncNhI/';
	$share_link = 'https://docs.google.com/spreadsheets/d/1cikiYnFJp2e-S-m9GVNG_BHLSAulHCeCO_qlWsncNhI/edit?usp=sharing';

	$url_with_auth = 'https://sheets.googleapis.com/v4/spreadsheets/'. $sheet_id  . '/values/Sheet1?key=' . $api_key;

	
	
	
	try {
	    $json = file_get_contents($url_with_auth);

	    if ($json === false) {
	        return 0;
	    } else {
	    	$obj = json_decode($json);
	    	return $obj;
	    }
	} catch (Exception $e) {
	    // Handle exception
	    return -1;
	}
}

function test($varname, $variable){
 echo $varname . ' ' . $variable;
}




// update excel_sheet_table(){
function update_excel_sheet_table(){

		$imported_data = get_excel_sheet();
		$sheet_rows_number = count($imported_data->values);
		if ($sheet_rows_number <= 0 ){ 
			echo ' sheet not imported';
		} else {
			echo 'table imported';
		}
		
		global $wpdb;
		$table_name = $wpdb->prefix . "excel_stock";
		$results = $wpdb->get_results( "SELECT * FROM ". $wpdb->prefix ."excel_stock");

		$table_rows_number = count($results);
		
		// check if data imported correctly
		if($sheet_rows_number-1 > $table_rows_number){

			if($table_rows_number == 0){
				$j = 1;
			} else {
				$j = $table_rows_number;
			}

			for( $i = $j ; $i < $sheet_rows_number;$i++){

				$wpdb->insert($table_name, 
					array(
						'sku' 				=> $imported_data->values[$i][0], // ['SKU'],
						'size' 				=> $imported_data->values[$i][1], // ['Size'],
						'stock' 			=> $imported_data->values[$i][2], // ['Amount In Stock'],
						'batch' 			=> $imported_data->values[$i][3], //['Batch'],
						'available_stock' 	=> $imported_data->values[$i][4], //['Currently In Stock']
						'is_added' 			=> '', //['Is Added']
						
				) ); 

			} //for
			

		} //if

}

//wc_get_product_id_by_sku



function add_to_stock(){

	global $wpdb;
	$table_name = $wpdb->prefix . "excel_stock";
	$results = $wpdb->get_results( "SELECT * FROM ". $wpdb->prefix ."excel_stock");



	foreach ($results as $result) {

		// if not added to stock
		if(!$result->is_added){
			$var_updated = false;

			
			$var_size = strtolower(str_replace(" ", "",$result->size));
			
			$var_batch = strtolower($result->batch);
			
			$var_stock = $result->stock;
			$var_price['50ml'] = '20.3';
			$var_price['100ml'] = '34.6';

			$result_product_id = wc_get_product_id_by_sku($result->sku);
			$result_product = wc_get_product( $result_product_id );
			$result_product_attributes = $result_product->get_attributes();
			$result_product_default_attributes = $result_product->get_default_attributes();
			$result_batch_values = $result_product->get_attribute('pa_batch-number');

			

			$product_variable = new WC_Product_Variable($result_product_id);
			$variations = $product_variable->get_available_variations();
			
			

			// add stock to existing variations
			foreach ($variations as $variation) {
				
				if($variation['attributes']['attribute_pa_batch-number'] == $var_batch && $variation['attributes']['attribute_pa_size'] == $var_size){
					//var_dump($variation);
					echo 'Product ' . $variation['attributes']['attribute_pa_batch-number'] . " " . $variation['attributes']['attribute_pa_size'] ." with stock ". $variation['max_qty'] . " was updated, ";

					// update variation stock
					$stock_after_update = wc_update_product_stock( $variation['variation_id'], $var_stock, $operation = 'increase' );
					
					// update excel_table 
					if($stock_after_update > 0){
						$wpdb->update( $table_name , array('is_added' => 'true'), array( 'ID' => $result->id ), array( '%s' ) );
						$var_updated = true;
						echo $var_stock . ' items added. ' . ' Current stock: ' . $stock_after_update . ' items' ;
					}

				}	
			}

			//create variation if it doesn't exist
			if($var_updated == false){

				$variation_data =  array(
				    'attributes' => array(
				        'size'  => $var_size,
				        'batch-number' => $var_batch,
				    ),
				    'sku'           => '',
				    'regular_price' => $var_price[$var_size],
				    'sale_price'    => '',
				    'stock_qty'     => $var_stock,
				);
				
				// check if variation exists
				$meta_query = array();
				foreach ($variation_data['attributes'] as $key => $value) {
				  $meta_query[] = array(
				    'key' => 'attribute_pa_' . $key,
				    'value' => $value
				  );
				}

				$variation_post = get_posts(array(
				  'post_type' => 'product_variation',
				  'numberposts' => 1,
				  'post_parent'   => $result_product_id,
				  'meta_query' =>  $meta_query
				));

				if($variation_post) {
				  $variation_data['variation_id'] = $variation_post[0]->ID;
				}

				create_product_variation( $result_product_id, $variation_data );
				echo 'Created Variation sku: ' . $result->sku . ' batch: ' . $var_batch . ' size: ' . $var_size . ' stock: ' . $var_stock;
				$wpdb->update( $table_name , array('is_added' => 'true'), array( 'ID' => $result->id ), array( '%s' ) );
				$var_updated = true;
			}


		}
			
		
	}





}


/**
 * Create a product variation for a defined variable product ID.
 *
 * @since 3.0.0
 * @param int   $product_id | Post ID of the product parent variable product.
 * @param array $variation_data | The data to insert in the product.
 */

function create_product_variation( $product_id, $variation_data ){
    

    if(isset($variation_data['variation_id'])) {

      $variation_id = $variation_data['variation_id'];

    } else {

      // if the variation doesn't exist then create it

      // Get the Variable product object (parent)
      $product = wc_get_product($product_id);

      $variation_post = array(
          'post_title'  => $product->get_title(),
          'post_name'   => 'product-'.$product_id.'-variation',
          'post_status' => 'publish',
          'post_parent' => $product_id,
          'post_type'   => 'product_variation',
          'guid'        => $product->get_permalink()
      );

      // Creating the product variation
      $variation_id = wp_insert_post( $variation_post );

    }

    // Get an instance of the WC_Product_Variation object
    $variation = new WC_Product_Variation( $variation_id );

    // Iterating through the variations attributes
    foreach ($variation_data['attributes'] as $attribute => $term_name )
    {
    	var_dump($attribute);
        $taxonomy = 'pa_'.$attribute; // The attribute taxonomy

        // Check if the Term name exist and if not we create it.
        if( ! term_exists( $term_name, $taxonomy ) )
            wp_insert_term( $term_name, $taxonomy ); // Create the term

        $term_slug = get_term_by('name', $term_name, $taxonomy )->slug; // Get the term slug

        // Get the post Terms names from the parent variable product.
        $post_term_names =  wp_get_post_terms( $product_id, $taxonomy, array('fields' => 'names') );

        // Check if the post term exist and if not we set it in the parent variable product.
        if( ! in_array( $term_name, $post_term_names ) )
            wp_set_post_terms( $product_id, $term_name, $taxonomy, true );

        // Set/save the attribute data in the product variation
        update_post_meta( $variation_id, 'attribute_'.$taxonomy, $term_slug );
    }

    ## Set/save all other data

    // SKU
    if( ! empty( $variation_data['sku'] ) )
        $variation->set_sku( $variation_data['sku'] );

    // Prices
    if( empty( $variation_data['sale_price'] ) ){
        $variation->set_price( $variation_data['regular_price'] );
    } else {
        $variation->set_price( $variation_data['sale_price'] );
        $variation->set_sale_price( $variation_data['sale_price'] );
    }
    $variation->set_regular_price( $variation_data['regular_price'] );

    // Stock
    if( ! empty($variation_data['stock_qty']) ){
        $variation->set_stock_quantity( $variation_data['stock_qty'] );
        $variation->set_manage_stock(true);
        $variation->set_stock_status('');
    } else {
        $variation->set_manage_stock(false);
    }

    $variation->set_weight(''); // weight (reseting)

    $variation->save(); // Save the data
}


function get_variation_stock( $product){

	$stock = 0;
	$parent_id = $product->get_parent_id();
	$attributes = $product->get_attributes();
	$product_variable = new WC_Product_Variable($parent_id);
	foreach ( $product_variable->get_children() as $child_id ) {
      $variation = wc_get_product( $child_id );
      
				
		if($variation->get_attributes()['pa_size'] == $attributes['pa_size']){

			$stock += $variation->get_stock_quantity();
		
		}
	}


	return $stock;
}



// sum all stocks for given size  
function filter_woocommerce_get_stock_html( $html, $product ) { 



    $html         = '';
    $availability = $product->get_availability();

    if ( $product->is_type( 'variation' ) ) {

    	$stock = get_variation_stock($product);
    	//$availability = get_variation_stock( $product_id, $size, $batch);
    	if($stock > 0)  {
    	 $availability['availability'] = $stock . ' in stock';
    	}

    } 

	if ( ! empty( $availability['availability'] ) ) {
		ob_start();

		wc_get_template( 'single-product/stock.php', array(
			'product'      => $product,
			'class'        => $availability['class'],
			'availability' => $availability['availability'],
		) );

		$html = ob_get_clean();
	}			
	

	return $html;
}; 
         
// add the filter 
add_filter( 'woocommerce_get_stock_html', 'filter_woocommerce_get_stock_html', 10, 2 ); 



function reduce_stock_in_excel_table( $order ) { // you get an object $order as an argument
	

	global $wpdb;
	$table_name = $wpdb->prefix . "excel_stock";
	$results = $wpdb->get_results( "SELECT * FROM ". $wpdb->prefix ."excel_stock");

	$items = $order->get_items();

	$items_ids = array();

	foreach ( $order->get_items() as $item_id => $item ) {

    // Here you get your data
    $custom_field = wc_get_order_item_meta( $item_id, '_tmcartepo_data', true ); 

    // To test data output (uncomment the line below)
    // print_r($custom_field);

    // If it is an array of values
    
	}

	foreach( $items as $item_id => $item ) {
		$items_ids[] = $item['product_id'];

		$product = wc_get_product( $item['product_id'] );
		$variation = new WC_Product_Variation( $item['variation_id'] );
		$attributes = $variation->get_attributes();
		$product_sku = $product->get_sku();
		// $product_size = $variation['attributes']['pa_size'];
		// $product_batch = $variation['attributes']['pa_bath-number'];

				
		
		$only_once = 0;
		
		foreach ($results as $result) {
			
			$table_sku = $result->sku;
			$table_size = strtolower(str_replace(" ", "",$result->size));
			$table_available_stock = intval($result->available_stock);

			if($product_sku == $table_sku && $only_once == 0 && $attributes['pa_size'] == $table_size && $table_available_stock >= $item['qty']){
				$table_stock = intval($table_available_stock) - $item['qty'];
				$wpdb->update( $table_name , array('available_stock' => $table_stock ), array( 'ID' => $result->id ), array( '%s' ) );
				$only_once = 1;
			}

		}
		$only_once = 0;
	}
  
  	
  	

	



}
add_action( 'woocommerce_reduce_order_stock', 'reduce_stock_in_excel_table' );



         

