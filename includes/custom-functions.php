<?php 

function ml_custom_login_logo() { ?>
    <style type="text/css">
        #login h1 a,
		.login h1 a {
            background-image: url('https://aromen.be/wp-content/uploads/2020/08/logonew600x150.png');
			height: 65px;
			width: 250px;
			background-size: contain;
			background-repeat: no-repeat;
        	padding-bottom: 30px;
        }
		body.login div#login .button-primary {
			background: #093b25;
			border-color: #093b25;
			color: #fff;
			text-decoration: none;
			text-shadow: none;
		}
		body.login div#login .button, 
		body.login div#login .button-secondary {
			color: #fff;
			border-color: #093b25;
			background: #093b25;
			vertical-align: top;
		}
		body.login div#login #login_error, 
		body.login div#login .message, 
		body.login div#login .success {
			border-left: 4px solid #093b25;
		}
		body.login div#login input[type=text]:focus,
		body.login div#login input[type=password]:focus,
		body.login div#login input[type=checkbox]:focus {
			border-color: #093b25;
    		box-shadow: 0 0 0 1px #093b25;
		}
		body.login div#login a {
			color: #093b25;
		}
    </style>
<?php 
function ml_custom_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'ml_custom_logo_url' );

function ml_custom_login_logo_url_title() {
    return 'Aromen BVBA';
}

add_filter( 'login_headertitle', 'ml_custom_login_logo_url_title' );
 }
add_action( 'login_enqueue_scripts', 'ml_custom_login_logo' );

function compare_by_batch($a, $b) {
  return strcmp($a['attributes']["attribute_pa_batch-number"], $b['attributes']["attribute_pa_batch-number"]);
}

function select_Variations($variations, $attribute, $value){

	$new_Array = [];

	foreach ($variations as $variation){

		if($variation['attributes'][$attribute] == $value){
			
			$new_Array[] = $variation;

		}

	}

	// sort variations by batch number
	usort($new_Array, 'compare_by_batch');

	return $new_Array;
}


function get_batch($sku, $size, $qty){

	$batch = '';
	global $wpdb;
	$table_name = $wpdb->prefix . "excel_stock";
	$results = $wpdb->get_results( "SELECT * FROM ". $wpdb->prefix ."excel_stock");

	foreach ($results as $result) {
		if($result->sku == $sku && strtolower(str_replace(" ", "",$result->size)) == strtolower(str_replace(" ", "",$size)) && intval($result->available_stock) >= intval($qty)){

			return $result->batch;

		}
	}

	return $batch ;

}
function get_batches($sku, $size, $qty){

	$batch = [];
	global $wpdb;
	$table_name = $wpdb->prefix . "excel_stock";
	$results = $wpdb->get_results( "SELECT * FROM ". $wpdb->prefix ."excel_stock");

	foreach ($results as $result) {
		if($result->sku == $sku && strtolower(str_replace(" ", "",$result->size)) == strtolower(str_replace(" ", "",$size)) && intval($result->available_stock) >0){

			$batch[] = $result->batch;

		}
	}

	return $batch ;

}
function select_batch_variation($variations, $size, $batch){

	$new_Array = [];

	foreach ($variations as $variation){

		if(strtolower($variation['attributes']['attribute_pa_batch-number']) == strtolower($batch) && strtolower($variation['attributes']['attribute_pa_size']) == strtolower($size)){
			
			$new_Array[] = $variation;

		}

	}

	// sort variations by batch number
	//usort($new_Array, 'compare_by_batch');

	return $new_Array;
}


//$aromen_quantity_to_add = 0;

function aromen_add_to_cart(){

	ob_start();
	
	$products['product_id'] = stripslashes($_POST['product_id']);
	$products['qty'] = stripslashes($_POST['product_qty']);
	$products['size'] = stripslashes($_POST['product_size']);
	
	
	
	// global $product;
	
	// if ( $product->is_type( 'variable' ) ) {


		$product_variable = new WC_Product_Variable($products['product_id']);
		$product_variations = $product_variable->get_available_variations();
		$product_sku = $product_variable->get_sku();
		$product_batch = get_batch($product_sku, $products['size'], $products['qty']);
		$product_batches = get_batches($product_sku, $products['size'], $products['qty']);
		// select variation with selected size 
		//$selected_variations = select_Variations($product_variations, 'attribute_pa_size', $products['size']);
		$selected_variations = select_batch_variation($product_variations, $products['size'], $product_batch);
		$count_selected_variations = count($selected_variations);

		$selected_variationss = [];

		foreach ($product_batches as $product_batch) {
			$selected_variationss[] = select_batch_variation($product_variations, $products['size'], $product_batch);
		}

		

		// check if there is enough products with one batch number
		// find one with enough quantity
		// $find_index = -1;
		// for ($i = 0; $i < $count_selected_variations; $i++) {

		// 	if($selected_variations[$i]['attributes']['attribute_pa_batch-number'] != 'default' && $products['qty'] <= $selected_variations[$i]['max_qty']){

		// 		$find_index = $i;

		// 	} 

		// }

		$find_index = 0;

 // $data = array(
 // 		'yy' => $count_selected_variations

 // );
	// 	  wp_send_json( $data );
		//if there is enough

		$cart_items = WC()->cart->get_cart_item_quantities();

		$variations_to_add = [];
		$quantity_to_add = $products['qty'];
		//global $aromen_quantity_to_add;
		//$aromen_quantity_to_add = $quantity_to_add;

		for( $i = 0 ; $i< count($selected_variationss);$i++ ){

			if($quantity_to_add  <= $selected_variationss[$i][0]['max_qty'] - WC()->cart->get_cart_item_quantities()[$selected_variationss[$i][0]['variation_id']]){
				$variations_to_add[$i]['var'] = $selected_variationss[$i];
				$variations_to_add[$i]['qty'] = $quantity_to_add ;
				break;


			} else if($selected_variationss[$i][0]['max_qty'] - WC()->cart->get_cart_item_quantities()[$selected_variationss[$i][0]['variation_id']] > 0 ) {
				$variations_to_add[$i]['var'] = $selected_variationss[$i];
				$variations_to_add[$i]['qty'] = $selected_variationss[$i][0]['max_qty'] - WC()->cart->get_cart_item_quantities()[$selected_variationss[$i][0]['variation_id']];
				$quantity_to_add -= $selected_variationss[$i][0]['max_qty'] - WC()->cart->get_cart_item_quantities()[$selected_variationss[$i][0]['variation_id']];

			}


		}
		
		//$aromen_quantity_to_add = $quantity_to_add;
		foreach ($variations_to_add as $variation_to_add) {

			WC()->cart->add_to_cart( $products['product_id'], $variation_to_add['qty'], $variation_to_add['var'][0]['variation_id'], (array) $variation_to_add['var'][0]['attributes'] );

		}
		do_action( 'woocommerce_ajax_added_to_cart', $products['product_id'] );
		
		if ( get_option( 'woocommerce_cart_redirect_after_add' ) == 'yes' ) {
            wc_add_to_cart_message( $products['product_id'] );
            }	
            WC_AJAX::get_refreshed_fragments();
       
		            /*
		
		if($products['qty'] <= $selected_variationss[$find_index][0]['max_qty'] - WC()->cart->get_cart_item_quantities()[$selected_variationss[$find_index][0]['variation_id']]){
			ob_start();
			// add to cart

			$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $products['product_id'], $products['qty'], $selected_variationss[$find_index][0]['variation_id'], (array) $selected_variationss[$find_index][0]['attributes'], $cart_item_data );

			if ( $passed_validation && WC()->cart->add_to_cart( $products['product_id'], $products['qty'], $selected_variationss[$find_index][0]['variation_id'], (array) $selected_variationss[$find_index][0]['attributes'] )){

				do_action( 'woocommerce_ajax_added_to_cart', $products['product_id'] );

				if ( get_option( 'woocommerce_cart_redirect_after_add' ) == 'yes' ) {
		            wc_add_to_cart_message( $products['product_id'] );

		        }

				WC_AJAX::get_refreshed_fragments();
				
				
			} else {

		        // If there was an error adding to the cart, redirect to the product page to show any errors
		        $data = array(
		            'error' => true,
		            'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $products['product_id'] ), $products['product_id'] )
		        );

		        wp_send_json( $data );

		    } 


		}  */

		
		
			

		// $data = array(
  //           'error' => true,
  //           'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $products['product_id'] ), $products['product_id'] ),
  //           'a' => $cart_items,
  //           'b' => $selected_variationss,
  //           'c' => intval($products['qty']),
  //           'd' => $cart_items[$selected_variationss[$find_index][0]['variation_id']]

  //       );

  //       wp_send_json( $data );
		
		
		
	die();
}

//add_action('wp_ajax_aromen_add_to_cart', 'aromen_add_to_cart');
//add_action('wp_ajax_nopriv_aromen_add_to_cart', 'aromen_add_to_cart');



function woocommerce_header_add_to_cart_fragment( $fragments ) { 
	global $woocommerce; 

	$cart_items = WC()->cart->get_cart_contents_count();
	$cart_summa = strip_tags(WC()->cart->get_cart_subtotal());

	ob_start(); ?>

	<span class="contact_cart_totals">
		<span class="cart_items"><?php
			echo esc_html($cart_items) . ' ' . ($cart_items == 1 ? esc_html__('Item', 'organic-beauty') : esc_html__('Items', 'organic-beauty'));
		?></span>
		- 
		<span class="cart_summa"><?php organic_beauty_show_layout($cart_summa); ?></span>
	</span>

	<?php 
	
	$fragments['.contact_cart_totals'] = ob_get_clean();
	
	//global $aromen_quantity_to_add;
	/*if($aromen_quantity_to_add >0){
		$fragments['.woocommerce-error'] ='Not enough items in stock to add ' . $aromen_quantity_to_add . ' more.';
		$aromen_quantity_to_add = 0;
	}*/
		
	 return $fragments; 
	 die();

 }// end ajax cart update

//add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment'); 
add_filter( 'woocommerce_add_to_cart_fragments', 'ml_woocommerce_header_add_to_cart_fragment' );

function ml_woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();

	?><div class="cart-customlocation"><?php get_template_part(organic_beauty_get_file_slug('templates/headers/_parts/contact-info-cart.php')); ?></div>
	
	<?php
	$fragments['.cart-customlocation'] = ob_get_clean();
	return $fragments;
}


// create table for storing data from excel sheet
function create_excel_stock_table(){

	global $wpdb;
	$table_name = $wpdb->prefix.'excel_stock';
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
	     //table not in database. Create new table
	     $charset_collate = $wpdb->get_charset_collate();
	 
	     $sql = "CREATE TABLE $table_name (
	          id mediumint(9) NOT NULL AUTO_INCREMENT,
	          sku text NOT NULL,	          
	          size text NOT NULL,	          
	          stock text NOT NULL,	          
	          batch text NOT NULL,	          
	          available_stock text NOT NULL,	          
	          is_added text NOT NULL,	          
	          UNIQUE KEY id (id)
	     ) $charset_collate;";
	     require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	     dbDelta( $sql );

	     //register the new table with the wpdb object
         if (!isset($wpdb->excel_stock))
         {
             $wpdb->excel_stock = $table_name;
             //add the shortcut so you can use $wpdb->stats
             $wpdb->tables[] = str_replace($wpdb->prefix, '', $table_name);
         }

	}


}
//add_action( 'init', 'create_excel_stock_table');


// add table view to admin dashboard
	
//add_action('admin_menu', 'add_excel_stock_page');
function add_excel_stock_page() {

add_menu_page( 'Excel Stock', 'Excel Stock', 'manage_options', 'excel-stock', 'excel_stock_page', $icon_url = '', $position = "6" );

}
function excel_stock_page(){

?>

<div>
<h2>Stocks from Excel</h2>

<?php 

	global $wpdb;
	$table_name = "excel_stock";
	$results = $wpdb->get_results( "SELECT * FROM ". $wpdb->prefix ."excel_stock");

	
	echo '<table class="widefat fixed" cellspacing="0">';

	echo '<thead><tr>
				<th id="ID" class="manage-column column-columnname num" scope="col">ID</th>
				<th id="SKU" class="manage-column column-columnname" scope="col">SKU</th>
				<th id="Size" class="manage-column column-columnname" scope="col">Size</th>
				<th id="Stock" class="manage-column column-columnname" scope="col">Stock</th>
				<th id="Batch" class="manage-column column-columnname" scope="col">Batch</th>
				<th id="Available" class="manage-column column-columnname" scope="col">Available</th>
				<th id="Is_Added" class="manage-column column-columnname" scope="col">Is Added</th>
					
			</tr></thead><tbody>';
	foreach ( $results as $result){

		?>

		<tr>
			<td><?php echo $result->id; ?></td>
			<td><?php echo $result->sku; ?></td>
			<td><?php echo $result->size; ?></td>
			<td><?php echo $result->stock; ?></td>
			<td><?php echo $result->batch; ?></td>
			<td><?php echo $result->available_stock; ?></td>
			<td><?php echo $result->is_added; ?></td>
		
		</tr>


		<?php
		
	}
 ?>
</tbody></table>
<?php  update_excel_sheet_table(); ?>
<?php add_to_stock(); ?>
</div>
 
<?php
}


// get excel sheet data

function get_excel_sheet(){
	$api_key = 'AIzaSyDbRhm81bjmB1MVDOgha8CjxeAIzq-_LXw';
	$sheet_id = '1cikiYnFJp2e-S-m9GVNG_BHLSAulHCeCO_qlWsncNhI';
	$url = 'https://docs.google.com/spreadsheets/d/1cikiYnFJp2e-S-m9GVNG_BHLSAulHCeCO_qlWsncNhI/';
	$share_link = 'https://docs.google.com/spreadsheets/d/1cikiYnFJp2e-S-m9GVNG_BHLSAulHCeCO_qlWsncNhI/edit?usp=sharing';

	$url_with_auth = 'https://sheets.googleapis.com/v4/spreadsheets/'. $sheet_id  . '/values/Sheet1?key=' . $api_key;

	
	
	
	try {
	    $json = file_get_contents($url_with_auth);

	    if ($json === false) {
	        return 0;
	    } else {
	    	$obj = json_decode($json);
	    	return $obj;
	    }
	} catch (Exception $e) {
	    // Handle exception
	    return -1;
	}
}


// update excel_sheet_table(){
function update_excel_sheet_table(){

		$imported_data = get_excel_sheet();
		$sheet_rows_number = count($imported_data->values);
		if ($sheet_rows_number <= 0 ){ 
			echo ' sheet not imported';
			return false;
		} else {
			echo 'sheet imported' . ' rows ' . $sheet_rows_number;
		}
		
		global $wpdb;
		$table_name = $wpdb->prefix . "excel_stock";
		$results = $wpdb->get_results( "SELECT * FROM ". $wpdb->prefix ."excel_stock");

		$table_rows_number = count($results);
		
		// check if data imported correctly
		if($sheet_rows_number-1 > $table_rows_number){

			if($table_rows_number == 0){
				$j = 1;
			} else {
				$j = $table_rows_number;
			}

			for( $i = $j ; $i < $sheet_rows_number;$i++){

				$wpdb->insert($table_name, 
					array(
						'sku' 				=> $imported_data->values[$i][0], // ['SKU'],
						'size' 				=> $imported_data->values[$i][1], // ['Size'],
						'stock' 			=> $imported_data->values[$i][2], // ['Amount In Stock'],
						'batch' 			=> $imported_data->values[$i][3], //['Batch'],
						'available_stock' 	=> $imported_data->values[$i][2], //['Currently In Stock']
						'is_added' 			=> '', //['Is Added']
						
				) ); 

			} //for
			echo ' , ' . $sheet_rows_number - $j . 'rows added';
			return true;

		} //if
		echo ', table is up to date';
		return true;

}

//wc_get_product_id_by_sku


function get_default_variation_price($product_id, $product_size){

	

	$product_variable = new WC_Product_Variable($product_id);
	$variations = $product_variable->get_available_variations();

	foreach ($variations as $variation) {
		
		if(isset($variation['attributes']['attribute_pa_batch-number']) && isset($variation['attributes']['attribute_pa_size']) && $variation['attributes']['attribute_pa_batch-number'] == 'default' && $variation['attributes']['attribute_pa_size'] == $product_size){
			
			return $variation['display_regular_price'];

		}
	}

	
	return false;
	

}

function add_to_stock(){

	global $wpdb;
	$table_name = $wpdb->prefix . "excel_stock";
	$results = $wpdb->get_results( "SELECT * FROM ". $wpdb->prefix ."excel_stock");



	foreach ($results as $result) {

		// if not added to stock
		if($result->is_added != "true"){
			$var_updated = false;
			$var_batch_updated;

			
			$var_size = strtolower(str_replace(" ", "",$result->size));			
			$var_batch = strtolower($result->batch);			
			$var_default_batch = 'default';			
			$var_stock = $result->stock;


			// $var_price['50ml'] = '20.3';
			// $var_price['100ml'] = '34.6';

			$result_product_id = wc_get_product_id_by_sku($result->sku);
			$result_product = wc_get_product( $result_product_id );
			$result_product_attributes = $result_product->get_attributes();
			$result_product_default_attributes = $result_product->get_default_attributes();
			$result_batch_values = $result_product->get_attribute('pa_batch-number');

			$var_price = get_default_variation_price($result_product_id, $var_size)?  get_default_variation_price($result_product_id, $var_size) : "noprice";
			
			$product_variable = new WC_Product_Variable($result_product_id);
			$variations = $product_variable->get_available_variations();
			
			

			// add stock to existing variations
			foreach ($variations as $variation) {
				
				if(isset($variation['attributes']['attribute_pa_batch-number']) && isset($variation['attributes']['attribute_pa_size']) && $variation['attributes']['attribute_pa_batch-number'] == $var_batch && $variation['attributes']['attribute_pa_size'] == $var_size){
					//var_dump($variation);
					echo 'Product ' . $variation['attributes']['attribute_pa_batch-number'] . " " . $variation['attributes']['attribute_pa_size'] ." with stock ". $variation['max_qty'] . " was updated, ";

					// update variation stock
					$stock_after_update = wc_update_product_stock( $variation['variation_id'], $var_stock, $operation = 'increase' );
					$var_updated = true;
					// update excel_table 
					if($stock_after_update > 0 && $var_batch_updated == true){
						$wpdb->update( $table_name , array('is_added' => 'true'), array( 'ID' => $result->id ), array( '%s' ) );
						
						echo $var_stock . ' items added. ' . ' Current stock: ' . $stock_after_update . ' items' ;
					}

				}
				if(isset($variation['attributes']['attribute_pa_batch-number']) && isset($variation['attributes']['attribute_pa_size']) && $variation['attributes']['attribute_pa_batch-number'] == $var_default_batch && $variation['attributes']['attribute_pa_size'] == $var_size){
					//var_dump($variation);
					echo 'Product ' . $variation['attributes']['attribute_pa_batch-number'] . " " . $variation['attributes']['attribute_pa_size'] ." with stock ". $variation['max_qty'] . " was updated, ";

					// update variation stock
					$stock_after_update = wc_update_product_stock( $variation['variation_id'], $var_stock, $operation = 'increase' );
					$var_batch_updated = true;
					// update excel_table 
					if($stock_after_update > 0 && $var_updated == true){
						$wpdb->update( $table_name , array('is_added' => 'true'), array( 'ID' => $result->id ), array( '%s' ) );
						
						echo $var_stock . ' items added. ' . ' Current stock: ' . $stock_after_update . ' items' ;
					}

				}	
			}

			//create variation if it doesn't exist
			if( $var_updated != "true" && $var_price != "noprice"){

				$variation_data =  array(
				    'attributes' => array(
				        'size'  => $var_size,
				        'batch-number' => strtoupper($var_batch),
				    ),
				    'sku'           => '',
				    'regular_price' => $var_price,
				    'sale_price'    => '',
				    'stock_qty'     => $var_stock,
				);
				
				// check if variation exists
				$meta_query = array();
				foreach ($variation_data['attributes'] as $key => $value) {
				  $meta_query[] = array(
				    'key' => 'attribute_pa_' . $key,
				    'value' => $value
				  );
				}

				$variation_post = get_posts(array(
				  'post_type' => 'product_variation',
				  'numberposts' => 1,
				  'post_parent'   => $result_product_id,
				  'meta_query' =>  $meta_query
				));

				if($variation_post) {
				  $variation_data['variation_id'] = $variation_post[0]->ID;
				}

				create_product_variation( $result_product_id, $variation_data );
				echo 'Created Variation sku: ' . $result->sku . ' batch: ' . $var_batch . ' size: ' . $var_size . ' stock: ' . $var_stock;
				$wpdb->update( $table_name , array('is_added' => 'true'), array( 'ID' => $result->id ), array( '%s' ) );
				$var_updated = true;
			} else if($var_updated != "true" && $var_price == "noprice"){

				$wpdb->update( $table_name , array('is_added' => 'no default value'), array( 'ID' => $result->id ), array( '%s' ) );
				$var_updated = true;
			}


		}
			
		
	}





}


/**
 * Create a product variation for a defined variable product ID.
 *
 * @since 3.0.0
 * @param int   $product_id | Post ID of the product parent variable product.
 * @param array $variation_data | The data to insert in the product.
 */

function create_product_variation( $product_id, $variation_data ){
    

    if(isset($variation_data['variation_id'])) {

      $variation_id = $variation_data['variation_id'];

    } else {

      // if the variation doesn't exist then create it

      // Get the Variable product object (parent)
      $product = wc_get_product($product_id);

      $variation_post = array(
          'post_title'  => $product->get_title(),
          'post_name'   => 'product-'.$product_id.'-variation',
          'post_status' => 'publish',
          'post_parent' => $product_id,
          'post_type'   => 'product_variation',
          'guid'        => $product->get_permalink()
      );

      // Creating the product variation
      $variation_id = wp_insert_post( $variation_post );

    }

    // Get an instance of the WC_Product_Variation object
    $variation = new WC_Product_Variation( $variation_id );

    // Iterating through the variations attributes
    foreach ($variation_data['attributes'] as $attribute => $term_name )
    {
    	var_dump($attribute);
        $taxonomy = 'pa_'.$attribute; // The attribute taxonomy

        // Check if the Term name exist and if not we create it.
        if( ! term_exists( $term_name, $taxonomy ) )
            wp_insert_term( $term_name, $taxonomy ); // Create the term

        $term_slug = get_term_by('name', $term_name, $taxonomy )->slug; // Get the term slug

        // Get the post Terms names from the parent variable product.
        $post_term_names =  wp_get_post_terms( $product_id, $taxonomy, array('fields' => 'names') );

        // Check if the post term exist and if not we set it in the parent variable product.
        if( ! in_array( $term_name, $post_term_names ) )
            wp_set_post_terms( $product_id, $term_name, $taxonomy, true );

        // Set/save the attribute data in the product variation
        update_post_meta( $variation_id, 'attribute_'.$taxonomy, $term_slug );
    }

    ## Set/save all other data

    // SKU
    if( ! empty( $variation_data['sku'] ) )
        $variation->set_sku( $variation_data['sku'] );

    // Prices
    if( empty( $variation_data['sale_price'] ) ){
        $variation->set_price( $variation_data['regular_price'] );
    } else {
        $variation->set_price( $variation_data['sale_price'] );
        $variation->set_sale_price( $variation_data['sale_price'] );
    }
    $variation->set_regular_price( $variation_data['regular_price'] );

    // Stock
    if( ! empty($variation_data['stock_qty']) ){
        $variation->set_stock_quantity( $variation_data['stock_qty'] );
        $variation->set_manage_stock(true);
        $variation->set_stock_status('');
    } else {
        $variation->set_manage_stock(false);
    }

    $variation->set_weight(''); // weight (reseting)

    $variation->save(); // Save the data
}


function get_variation_stock( $product){

	$stock = 0;
	$parent_id = $product->get_parent_id();
	$attributes = $product->get_attributes();
	$product_variable = new WC_Product_Variable($parent_id);
	foreach ( $product_variable->get_children() as $child_id ) {
      $variation = wc_get_product( $child_id );
      
				
		if($variation->get_attributes()['pa_size'] == $attributes['pa_size']){

			$stock += $variation->get_stock_quantity();
		
		}
	}


	return $stock;
}



// sum all stocks for given size  
function filter_woocommerce_get_stock_html( $html, $product ) { 



    $html         = '';
    $availability = $product->get_availability();

    if ( $product->is_type( 'variation' ) ) {

    	$stock = get_variation_stock($product);
    	//$availability = get_variation_stock( $product_id, $size, $batch);
    	if($stock > 0)  {
    	 $availability['availability'] = $stock . ' in stock';
    	} else {
    		 $availability['availability'] = 'Out of stock';
    	}

    } 

	if ( ! empty( $availability['availability'] ) ) {
		ob_start();

		wc_get_template( 'single-product/stock.php', array(
			'product'      => $product,
			'class'        => $availability['class'],
			'availability' => $availability['availability'],
		) );

		$html = ob_get_clean();
	}			
	

	return $html;
}; 
         
// add the filter 
//add_filter( 'woocommerce_get_stock_html', 'filter_woocommerce_get_stock_html', 10, 2 ); 



function reduce_stock_in_excel_table( $order ) { // you get an object $order as an argument
	

	global $wpdb;
	$table_name = $wpdb->prefix . "excel_stock";
	$results = $wpdb->get_results( "SELECT * FROM ". $wpdb->prefix ."excel_stock");

	$items = $order->get_items();

	$items_ids = array();

	foreach ( $order->get_items() as $item_id => $item ) {

    // Here you get your data
    $custom_field = wc_get_order_item_meta( $item_id, '_tmcartepo_data', true ); 

    // To test data output (uncomment the line below)
    // print_r($custom_field);

    // If it is an array of values
    
	}

	foreach( $items as $item_id => $item ) {
		$items_ids[] = $item['product_id'];

		$product = wc_get_product( $item['product_id'] );
		$variation = new WC_Product_Variation( $item['variation_id'] );
		$attributes = $variation->get_attributes();
		$product_sku = $product->get_sku();
		// $product_size = $variation['attributes']['pa_size'];
		// $product_batch = $variation['attributes']['pa_bath-number'];

				
		
		$only_once = 0;
		
		foreach ($results as $result) {
			
			$table_sku = $result->sku;
			$table_size = strtolower(str_replace(" ", "",$result->size));
			$table_available_stock = intval($result->available_stock);

			if($product_sku == $table_sku && $only_once == 0 && $attributes['pa_size'] == $table_size && $table_available_stock >= $item['qty']){
				$table_stock = intval($table_available_stock) - $item['qty'];
				$wpdb->update( $table_name , array('available_stock' => $table_stock ), array( 'ID' => $result->id ), array( '%s' ) );
				$only_once = 1;
			}

		}
		$only_once = 0;
	}
  
  	
  	

	



}
//add_action( 'woocommerce_reduce_order_stock', 'reduce_stock_in_excel_table' );


//add_action('woocommerce_checkout_process','aromen_update_cart',5,1);
//add_action( 'woocommerce_proceed_to_checkout', 'aromen_update_cart', 10, 2 ); 
//add_action( 'woocommerce_checkout_before_order_review', 'aromen_update_cart', 10, 2 ); 

function aromen_update_cart(){

	$cart_items = WC()->cart->get_cart_contents();

	// var_dump($cart_items );

	foreach ($cart_items as $cart_item_key => $cart_item) {

		echo $cart_item['product_id'];
		echo $cart_item['variation']['attribute_pa_size'];
		echo $cart_item['variation']['attribute_pa_batch-number'];
		echo $cart_item['quantity'];

		$version = '';
		if($cart_item['variation']['attribute_pa_batch-number'] == 'default'){
		$version = 'default';
		} else if($cart_item['variation']['attribute_pa_batch-number'] == 'default-de'){
			$version = 'default-de';
		}
		// remove_cart_item( string $cart_item_key )
		// add_to_cart( integer $product_id = 0, integer $quantity = 1, integer $variation_id = 0, array $variation = array(), array $cart_item_data = array()  )

		if($cart_item['variation']['attribute_pa_batch-number'] == $version ){

			$products['product_id'] = $cart_item['product_id'];
			$products['qty'] 		= $cart_item['quantity'];
			$products['size'] 		= $cart_item['variation']['attribute_pa_size'];
			


			$product_variable = new WC_Product_Variable($products['product_id']);
			$product_variations = $product_variable->get_available_variations();
			$product_sku = $product_variable->get_sku();
			
			$product_batch = get_batch($product_sku, $products['size'], $products['qty']);
			//$product_batches = get_batches($product_sku, $products['size'], $products['qty']);	
			
			$product_batches = array_values(array_diff($product_variable->get_variation_attributes()['pa_batch-number'], [$version ]));	
				
			
			$selected_variations = select_batch_variation($product_variations, $products['size'], $product_batch);
			$count_selected_variations = count($selected_variations);

			$selected_variationss = [];

			foreach ($product_batches as $product_batch) {
				$selected_variationss[] = select_batch_variation($product_variations, $products['size'], $product_batch);
			}

			//$cart_items = WC()->cart->get_cart_item_quantities();

			$variations_to_add = [];
			$quantity_to_add = $products['qty'];
			//global $aromen_quantity_to_add;
			//$aromen_quantity_to_add = $quantity_to_add;

			for( $i = 0 ; $i< count($selected_variationss);$i++ ){

				if($quantity_to_add  <= $selected_variationss[$i][0]['max_qty']){
					$variations_to_add[$i]['var'] = $selected_variationss[$i];
					$variations_to_add[$i]['qty'] = $quantity_to_add ;
					break;


				} else if($selected_variationss[$i][0]['max_qty'] > 0 ) {
					$variations_to_add[$i]['var'] = $selected_variationss[$i];
					$variations_to_add[$i]['qty'] = $selected_variationss[$i][0]['max_qty'];
					$quantity_to_add -= $selected_variationss[$i][0]['max_qty'];

				}


			}
			foreach ($variations_to_add as $variation_to_add) {

				WC()->cart->add_to_cart( $products['product_id'], $variation_to_add['qty'], $variation_to_add['var'][0]['variation_id'], (array) $variation_to_add['var'][0]['attributes'] );

			}
			$stock_after_update = wc_update_product_stock( $cart_item['variation_id'], $cart_item['quantity'], $operation = 'decrease' );
			WC()->cart->remove_cart_item( $cart_item_key );

			

		}
	}

}



         

/** 
*
*  add hscode to item metas 
*  
* 
* 
* @since 4.9.6
*/

function ml_display_item_meta( $html, $item, $args ) {

		
		$product       = $item->get_product();
		$prod_id 		   = $product->get_parent_id();
		

        $strings = array();
        $html    = '';
        $args    = wp_parse_args( $args, array(
            'before'    => '<ul class="wc-item-meta"><li>',
            'after'     => '</li></ul>',
            'separator' => '</li><li>',
            'echo'      => true,
            'autop'     => false,
        ) );

        foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) {
            $value     = $args['autop'] ? wp_kses_post( $meta->display_value ) : wp_kses_post( make_clickable( trim( $meta->display_value ) ) );
            $strings[] = '<strong class="wc-item-meta-label">' . wp_kses_post( $meta->display_key ) . ':</strong> ' . $value;
        }
        if(get_field('hscode', $prod_id)){
        	$strings[] = '<strong class="wc-item-meta-label">HS Code: </strong> ' . get_field('hscode', $prod_id);
        }
        	

        if ( $strings ) {
            $html = $args['before'] . implode( $args['separator'], $strings ) . $args['after'];
        }

       

        return $html; 
    }
	add_filter( 'woocommerce_display_item_meta', 'ml_display_item_meta',10,3 );



	function sv_unrequire_wc_phone_field( $fields ) {
		$fields['billing_vat']['required'] = false;
		return $fields;
	}
	//add_filter( 'woocommerce_billing_fields', 'sv_unrequire_wc_phone_field' );

	
	/**
 * @snippet       Remove Tax @ Checkout if Field Value Exists - WooCommerce
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=21952
 * @author        Rodolfo Melogli
 * @compatible    WC 3.2.6
 */
 
//add_action( 'woocommerce_checkout_update_order_review', 'bbloomer_taxexempt_checkout_based_on_zip' );
 
function bbloomer_taxexempt_checkout_based_on_zip( $post_data ) {
        global $woocommerce;
        $woocommerce->customer->set_is_vat_exempt( false );
		parse_str($post_data);
	
		if ( !empty($billing_vat) ) $woocommerce->customer->set_is_vat_exempt( true );
		
}


// add_action( 'woocommerce_single_product_summary','ml_display_main_price',11);

function ml_display_main_price(){
	$product = $_product = wc_get_product( get_the_id() );
	$min_variation_price = $product->get_variation_regular_price( 'max');
	echo $min_variation_price;
}

function ml_email_order_meta( $order, $sent_to_admin = false, $plain_text = false ) {
    $fields = apply_filters( 'woocommerce_email_order_meta_fields', array(), $sent_to_admin, $order );

    
    $_fields = apply_filters( 'woocommerce_email_order_meta_keys', array(), $sent_to_admin );

    if ( $_fields ) {
      foreach ( $_fields as $key => $field ) {
        if ( is_numeric( $key ) ) {
          $key = $field;
        }

        $fields[ $key ] = array(
          'label' => wptexturize( $key ),
          'value' => wptexturize( get_post_meta( $order->get_id(), $field, true ) ),
        );
      }
    }

    if ( $fields ) {

      if ( $plain_text ) {

        foreach ( $fields as $field ) {
          if ( isset( $field['label'] ) && isset( $field['value'] ) && $field['value'] ) {
            echo $field['label'] . ': ' . $field['value'] . "\n"; // WPCS: XSS ok.
          }
        }
      } else {

        foreach ( $fields as $field ) {
          if ( isset( $field['label'] ) && isset( $field['value'] ) && $field['value'] ) {
            echo '<p><strong>' . $field['label'] . ':</strong> ' . $field['value'] . '</p>'; // WPCS: XSS ok.
          }
        }
      }
    }
  }


//add_filter( 'authenticate', 'chk_active_user',100,2);
function chk_active_user ($user,$username) {
    $user_data = $user->data;
	$user_id = $user_data->ID;

	$wcb2b_status = get_the_author_meta( 'wcb2b_status', $user_id, true );
	
	
	
	
	if ( $user == null ) {
			// TODO what should the error message be? (Or would these even happen?)
			// Only needed if all authentication handlers fail to return anything.
		$user = new WP_Error( 'authentication_failed', __( '<strong>ERROR</strong>: Invalid username, email address or incorrect password.' ) );
	}
		
	$ignore_codes = array( 'empty_username', 'empty_password' );
			
	if ( is_wp_error( $user ) && ! in_array( $user->get_error_code(), $ignore_codes ) ) {
			/**
			 * Fires after a user login has failed.
				 *
			 * @since 2.5.0
				 * @since 4.5.0 The value of `$username` can now be an email address.
			 *
				 * @param string $username Username or email address.
			 */
				do_action( 'wp_login_failed', $username );
		} else if(user_can($user_id, 'administrator')){

		return $user;
		
		} else if (0 == (int)$wcb2b_status){

		return new WP_Error( 'disabled_account',"This account isn't active yet");

	} 	
		          
	
    return $user;
}

//add_filter( 'wcb2b_new_account_email', 'ml_echo_nothing' );

function ml_echo_nothing(){
	echo '<p> </p>';
}

/*
//1. Add a new form element...
add_action( 'woocommerce_register_form_start', 'business_register_form',5 );
function business_register_form() {

$busines_type = ( ! empty( $_POST['busines_type'] ) ) ? sanitize_text_field( $_POST['busines_type'] ) : '';

    ?>
    <p class="form-row form-row-wide business-type">
        <label for="business_type"><?php _e( 'Business Type', 'aromen' ) ?><br />
            <select type="text" name="business_type" id="busines_type" class="input">
			<option value="AX"><?php echo __('Wellnesscenter','aromen'); ?></option>
			<option value="AX"><?php echo __('Wellnessprofessional','aromen'); ?></option>
			<option value="AX"><?php echo __('Aufgussmaster','aromen'); ?></option>
			</select>
			</label>
    </p>
    <?php
}

//2. Add validation. In this case, we make sure busines_type is required.
add_filter( 'woocommerce_register_post', 'myplugin_registration_errors', 10, 3 );
function myplugin_registration_errors( $errors, $sanitized_user_login, $user_email ) {

    if ( empty( $_POST['busines_type'] ) || ! empty( $_POST['busines_type'] ) && trim( $_POST['busines_type'] ) == '' ) {
    // $errors->add( 'busines_type_error', __( 'You must include a business type.', 'aromen' ));

    }

    return $errors;
}

//3. Finally, save our extra registration user meta.
add_action( 'woocommerce_created_customer', 'myplugin_user_register' );
function myplugin_user_register( $user_id ) {
    if ( ! empty( $_POST['busines_type'] ) ) {
        update_user_meta( $user_id, 'busines_type', sanitize_text_field( $_POST['busines_type'] ) );
    }
}
*/

/*
function woocommerce_edit_my_account_page() {
    return apply_filters( 'woocommerce_forms_field', array(
        'woocommerce_my_account_page_select'     => array(
			'type'    => 'select',
			'label'   => __( 'Business Type', 'aromen' ),
			'required'=> true,
			'options' => array(
				'' => __( 'Select an options.', 'aromen' ),
				1  => __( 'Wellnesscenter', 'aromen' ),
				2  => __( 'Wellnessprofessional', 'aromen' ),
				3  => __( 'Aufgussmaster', 'aromen' ),
			),
    )));
}

function edit_my_account_page_woocommerce() {
    $fields = woocommerce_edit_my_account_page();
    foreach ( $fields as $key => $field_args ) {
        woocommerce_form_field( $key, $field_args );
    }
}
add_action( 'woocommerce_register_form', 'edit_my_account_page_woocommerce', 5 );
add_action( 'woocommerce_edit_account_form', 'edit_my_account_page_woocommerce', 5 );

function iconic_print_user_admin_fields() {
    $fields = woocommerce_edit_my_account_page();
    ?>
    <h2><?php _e( 'Additional Information', 'aromen' ); ?></h2>
    <table class="form-table" id="additional-information-business-type">
        <tbody>
        <?php foreach ( $fields as $key => $field_args ) { ?>
            <tr>
                <th>
                    <label for="<?php echo $key; ?>"><?php echo $field_args['label']; ?></label>
                </th>
                <td>
                    <?php $field_args['label'] = false; ?>
                    <?php woocommerce_form_field( $key, $field_args ); ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?php
}
 
add_action( 'show_user_profile', 'iconic_print_user_admin_fields', 30 ); // admin: edit profile
add_action( 'edit_user_profile', 'iconic_print_user_admin_fields', 30 ); // admin: edit other users


function iconic_save_account_fields( $customer_id ) {
	$fields = woocommerce_edit_my_account_page();
	$sanitized_data = array();

	foreach ( $fields as $key => $field_args ) {
		

		$sanitize = isset( $field_args['sanitize'] ) ? $field_args['sanitize'] : 'wc_clean';
		$value    = isset( $_POST[ $key ] ) ? call_user_func( $sanitize, $_POST[ $key ] ) : '';

			

		update_user_meta( $customer_id, $key, $value );
	}

	if ( ! empty( $sanitized_data ) ) {
		$sanitized_data['ID'] = $customer_id;
		wp_update_user( $sanitized_data );
	}
}

add_action( 'woocommerce_created_customer', 'iconic_save_account_fields' ); // register/checkout
add_action( 'personal_options_update', 'iconic_save_account_fields' ); // edit own account admin
add_action( 'edit_user_profile_update', 'iconic_save_account_fields' ); // edit other account admin
add_action( 'woocommerce_save_account_details', 'iconic_save_account_fields' ); // edit WC account*/

//add_action( 'woocommerce_register_form', 'bbloomer_extra_register_select_field', 5 );
 
function bbloomer_extra_register_select_field() {
   
    ?>
 
<p class="form-row form-row-wide">
<label for="business_type"><?php _e( 'I am a', 'woocommerce' ); ?>  <span class="required">*</span></label>
<select name="business_type" id="business_type" >
    <option value><?php echo __( 'Select an options.', 'aromen' ); ?></option>
    <option value="Wellnesscenter"><?php echo __( 'Wellnesscenter', 'aromen' ); ?></option>
    <option value="Wellnessprofessional"><?php echo __( 'Wellnessprofessional', 'aromen' ); ?></option>
    <option value="Aufgussmaster"><?php echo __( 'Aufgussmaster', 'aromen' ); ?></option>	
</select>
</p>
 
<?php
   
}
 
// -------------------
// 2. Save field on Customer Created action
 
add_action( 'woocommerce_created_customer', 'bbloomer_save_extra_register_select_field' );
  
function bbloomer_save_extra_register_select_field( $customer_id ) {
if ( isset( $_POST['business_type'] ) ) {
        update_user_meta( $customer_id, 'business_type', $_POST['business_type'] );
}
}
 
// -------------------
// 3. Display Select Field @ User Profile (admin) and My Account Edit page (front end)
  
//add_action( 'show_user_profile', 'bbloomer_show_extra_register_select_field', 30 );
//add_action( 'edit_user_profile', 'bbloomer_show_extra_register_select_field', 30 ); 
//add_action( 'woocommerce_edit_account_form', 'bbloomer_show_extra_register_select_field', 30 );
  
function bbloomer_show_extra_register_select_field($user){ 
   
  if (empty ($user) ) {
  $user_id = get_current_user_id();
  $user = get_userdata( $user_id );
  }
   
?>    
       
<p class="form-row form-row-wide">
<label for=""><?php _e( 'Business Type', 'woocommerce' ); ?>  <span class="required">*</span></label>
<select name="business_type" id="business_type" />
    <option value> -- select an option -- </option>
    <option value="goo" <?php if (get_the_author_meta( 'business_type', $user->ID ) == "Wellnesscenter") echo 'selected="selected" '; ?>><?php echo __( 'Wellnesscenter', 'aromen' ); ?></option>
    <option value="fcb" <?php if (get_the_author_meta( 'business_type', $user->ID ) == "Wellnessprofessional") echo 'selected="selected" '; ?>><?php echo __( 'Wellnessprofessional', 'aromen' ); ?></option>
    <option value="twt" <?php if (get_the_author_meta( 'business_type', $user->ID ) == "Aufgussmaster") echo 'selected="selected" '; ?>><?php echo __( 'Aufgussmaster', 'aromen' ); ?></option>
</select>
</p>
 
<?php
 
}
 
// -------------------
// 4. Save User Field When Changed From the Admin/Front End Forms
  
add_action( 'personal_options_update', 'bbloomer_save_extra_register_select_field_admin' );    
add_action( 'edit_user_profile_update', 'bbloomer_save_extra_register_select_field_admin' );   
add_action( 'woocommerce_save_account_details', 'bbloomer_save_extra_register_select_field_admin' );
  
function bbloomer_save_extra_register_select_field_admin( $customer_id ){
if ( isset( $_POST['business_type'] ) ) {
   update_user_meta( $customer_id, 'business_type', $_POST['business_type'] );
}
}

add_action( 'woocommerce_cart_actions', 'woocommerce_empty_cart_button',5 );
function woocommerce_empty_cart_button() {
	echo '<a href="' . esc_url( add_query_arg( 'empty_cart', 'yes' ) ) . '" class="no-button" title="' . __( 'Empty Cart', 'aromen' ) . '">' . __( 'Empty Cart', 'aromen' ) . '</a>';
}

add_action( 'wp_loaded', 'woocommerce_empty_cart_action', 20 );
function woocommerce_empty_cart_action() {
	if ( isset( $_GET['empty_cart'] ) && 'yes' === esc_html( $_GET['empty_cart'] ) ) {
		WC()->cart->empty_cart();

		$referer  = wp_get_referer() ? esc_url( remove_query_arg( 'empty_cart' ) ) : wc_get_cart_url();
		wp_safe_redirect( $referer );
	}
}
add_action('woocommerce_cart_collaterals','woocommerce_add_nice_bag',5);
function woocommerce_add_nice_bag() {
	echo '<div class="bag"><div class="bag-wrapper"><img class="bag-image" src="'. get_stylesheet_directory_uri().'/assets/img/bag-aromen.jpg"/></div></div>';
}

add_filter( 'woocommerce_email_recipient_customer_completed_order', 'your_email_recipient_filter_function', 10, 2);

function your_email_recipient_filter_function($recipient, $object) {
    $recipient .= ', administratie@aromen.be';
    return $recipient;
}




//add_action('wp_footer','quick_shop_badge');
function quick_shop_badge(){
	echo '<a href="/quick-order" class="badge-container" tilte="'.__('All products on one page in a table').'"><span class="rotated_text">' . __('Quick Shop') . '</span><span class="desc">'. __('For fast and easy orders').'</div></a>';
}


function disable_shipping_calc_on_cart( $show_shipping ) {
    if( is_cart() ) {
        return false;
    }
    return $show_shipping;
}
add_filter( 'woocommerce_cart_ready_to_calc_shipping', 'disable_shipping_calc_on_cart', 99 );

function display_additional_info(){
	//echo '<span style="display:none;">test</span>';
	global $product;
	 do_action( 'woocommerce_product_additional_information', $product );
}
add_action('woocommerce_product_meta_start','display_additional_info',99);


//add_action('woocommerce_after_shop_loop_item_title', 'display_additional_info');
//remove_action('woocommerce_after_shop_loop_item_title', 'organic_beauty_woocommerce_product_cats', 1);
add_action('woocommerce_after_shop_loop_item_title', 'woo_show_sizes',1);
//add_action('woocommerce_after_shop_loop_item_title', 'organic_beauty_woocommerce_product_cats', 2);

function woo_show_sizes(){
	global $product;
	$attributes = $product->get_attributes();
	foreach ( $attributes as $attribute ) { 
		if($attribute->get_name() == 'pa_size'){
			
			$terms = $attribute->get_terms();
			$displayed_terms = '';
			
			 foreach($terms as $term){
				 if($term->name !='1pc'){
					$displayed_terms .= $term->name . ' ';
				 }
				
			}
			if('' != $displayed_terms){
				echo '<span class="display-sizes">Size: ';
				echo $displayed_terms;
				echo '</span>';
			}
			
		}
		 
	

	}
	
}

add_action('woocommerce_after_shop_loop_item_title', 'aromen_product_cats',1);

// Add Product categories
if ( !function_exists( 'aromen_product_cats' ) ) {
	function aromen_product_cats()
	{
		global $post;
		$post_id = $post->ID;
		$post_cats = wp_get_post_terms($post_id, 'product_cat');
		$cats_out = '';
		$i = 0;

		if (!empty($post_cats)) {
			$count_cats = count($post_cats);
			foreach ($post_cats as $term) {
				$i++;
				$term_link = get_term_link($term, 'product_cat');
				$cats_out = get_post_meta($post->ID, '_yoast_wpseo_primary_category',true);
					// this is a primary category
				$cats_out .= !empty($term_link) ?  '<a href="' . esc_url($term_link) . '">' . esc_html($term->name) . '</a>' : '';
				$cats_out .= $i < $count_cats ? ', ' : '';
				$cats_out .= count($post_cats) > 1 && $i < count($post_cats) ? '' : '';
				 
				

			}
		}

		echo '<div class="product_cats">';
		echo(!empty($cats_out) ? $cats_out : '');
		echo '</div>';
		
	}
}
// add mobile menu
function register_mobile_menu() {
	register_nav_menus(
	  array(
		'mobile-menu' => __( 'Mobile Bottom Menu' ),
	  )
	);
  }
  add_action( 'init', 'register_mobile_menu' );

function ml_product_trees(){
	global $product;
	$id = $product->get_id();
	$trees = (!empty(get_field('trees', $id)))? get_field('trees', $id) : '1';
	
	echo '<div class="product-trees" ><div class="trees-number"><span class="num">+ ' . $trees . '</span> <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M23.25 11.0156C22.7344 10.2656 22.0781 9.75 21.2344 9.375H21.2812C21.6562 8.625 21.7969 7.875 21.7031 7.07812C21.6094 6.28125 21.2812 5.57812 20.7656 5.01562C20.2031 4.45312 19.5469 4.03125 18.7969 3.84375C18.0469 3.65625 17.25 3.75 16.5 4.07812C16.3594 2.95312 15.8438 1.96875 15 1.17188C14.1562 0.421875 13.125 0 12 0C10.8281 0 9.79688 0.421875 8.95312 1.17188C8.10938 1.96875 7.59375 2.95312 7.5 4.07812C6.70312 3.75 5.90625 3.65625 5.15625 3.84375C4.40625 4.03125 3.75 4.45312 3.23438 5.01562C2.67188 5.57812 2.34375 6.28125 2.25 7.07812C2.15625 7.875 2.29688 8.625 2.76562 9.375C1.92188 9.75 1.21875 10.2656 0.75 11.0156C0.234375 11.7656 0 12.6094 0 13.5C0 14.7656 0.421875 15.8438 1.3125 16.6875C2.15625 17.5781 3.23438 18 4.5 18H10.5V20.6719L9.04688 22.9219C8.8125 23.4375 9.1875 24.0469 9.75 24.0469H14.2031C14.7656 24.0469 15.1406 23.4375 14.9062 22.9219L13.5 20.6719V18H19.5C20.7188 18 21.7969 17.5781 22.6875 16.6875C23.5312 15.8438 24 14.7656 24 13.5C24 12.6094 23.7188 11.7656 23.25 11.0156Z" fill="#093B25"></path>
</svg></div><span class="desc"> ' . __("will be planted", "aromen") . '.</span></div>';
}
add_action('woocommerce_after_add_to_cart_button','ml_product_trees',99);

function ml_checkout_trees(){
	
	$trees = 0;
	foreach( WC()->cart->get_cart() as $cart_item ){
		$product_id = $cart_item['product_id'];
		$trees += (!empty(get_field('trees', $product_id)))? get_field('trees', $product_id) * $cart_item['quantity'] : $cart_item['quantity'];
		
	}
	echo '<div class="trees-wrapper"><span class="before-trees" >' . __("With your purchase, you’ll donate:",'aromen') . '</span><div class="product-trees" >
	<div class="trees-number"><span class="num">' . $trees . '</span> <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M23.25 11.0156C22.7344 10.2656 22.0781 9.75 21.2344 9.375H21.2812C21.6562 8.625 21.7969 7.875 21.7031 7.07812C21.6094 6.28125 21.2812 5.57812 20.7656 5.01562C20.2031 4.45312 19.5469 4.03125 18.7969 3.84375C18.0469 3.65625 17.25 3.75 16.5 4.07812C16.3594 2.95312 15.8438 1.96875 15 1.17188C14.1562 0.421875 13.125 0 12 0C10.8281 0 9.79688 0.421875 8.95312 1.17188C8.10938 1.96875 7.59375 2.95312 7.5 4.07812C6.70312 3.75 5.90625 3.65625 5.15625 3.84375C4.40625 4.03125 3.75 4.45312 3.23438 5.01562C2.67188 5.57812 2.34375 6.28125 2.25 7.07812C2.15625 7.875 2.29688 8.625 2.76562 9.375C1.92188 9.75 1.21875 10.2656 0.75 11.0156C0.234375 11.7656 0 12.6094 0 13.5C0 14.7656 0.421875 15.8438 1.3125 16.6875C2.15625 17.5781 3.23438 18 4.5 18H10.5V20.6719L9.04688 22.9219C8.8125 23.4375 9.1875 24.0469 9.75 24.0469H14.2031C14.7656 24.0469 15.1406 23.4375 14.9062 22.9219L13.5 20.6719V18H19.5C20.7188 18 21.7969 17.5781 22.6875 16.6875C23.5312 15.8438 24 14.7656 24 13.5C24 12.6094 23.7188 11.7656 23.25 11.0156Z" fill="#093B25"></path>
</svg></div></div><div class="after-trees">' . __( 'Your support aids the Gate to Nature foundation in rebuilding the forests in Kenya.Find more information about the reforestation project <a href="https://aromen.be/gatetonature/" target="_blank" rel="noopener noreferrer">here.</a>', 'aromen' ) . '</div></div>';
	

}
add_action('woocommerce_review_order_before_payment', 'ml_checkout_trees', 10);


add_filter( 'shortcode_atts_wpcf7', 'custom_shortcode_atts_wpcf7_filter', 10, 3 );
 
function custom_shortcode_atts_wpcf7_filter( $out, $pairs, $atts ) {
  $group = 'group';
 	$webinar = 'webinar';
  if ( isset( $atts[$group] ) ) {
    $out[$group] = $atts[$group];
  }
  if ( isset( $atts[$webinar] ) ) {
    $out[$webinar] = $atts[$webinar];
  }
 
  return $out;
}
function wcs_my_free_shipping( $is_available ) {
	global $woocommerce;
 
		// get cart contents
	$cart_items = $woocommerce->cart->get_cart();
	$class_price = 0;

	// loop through the items looking for one in the eligible array
	foreach ( $cart_items as $item ) {
		$shipping_class = $item['data']->get_shipping_class();
		$price = $item['data']->get_price();
		
		if( $shipping_class !== 'sweater' ) {
			$class_price += floatval($price) * $item['quantity'] ;
			
		}
		
	}
	if (floatval($class_price) > 50) {
		$is_available = true;
	}  else {
		$is_available = false;

	}
 
	// nothing found return the default value
	return $is_available;
}
add_filter( 'woocommerce_shipping_free_shipping_is_available', 'wcs_my_free_shipping', 20 );

/**
 * Hide shipping rates when free shipping is available.
 * Updated to support WooCommerce 2.6 Shipping Zones.
 *
 * @param array $rates Array of rates found for the package.
 * @return array
 */
function my_hide_shipping_when_free_is_available( $rates ) {
	$free = array();
	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id ) {
			$free[ $rate_id ] = $rate;
			break;
		}
	}
	return ! empty( $free ) ? $free : $rates;
}
add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );

function woo_jotpo_reviews(){
	echo '<!-- wc_yotpo_show_widget -->';
    wc_yotpo_show_widget();
}
add_action('woocommerce_after_single_product','woo_jotpo_reviews');
