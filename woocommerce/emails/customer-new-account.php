<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
<?php $user = get_user_by( 'login',$user_login ); ?>


<?php /* translators: %s Customer username */ ?>
<p><?php echo esc_html__( 'Dear', 'woocommerce' );?> <?php echo $user->get('display_name'); ?>,</p>
<?php /* translators: %1$s: Site title, %2$s: Username, %3$s: My account link */ ?>
<p><?php printf( __('Thank you for creating an account on')); ?> <a href="https://aromen.be/">Aromen.be</a></p>
<p><?php printf( __( 'Login: %1$s', 'woocommerce' ),  '<strong>' . esc_html( $user->get('user_email') ) . '</strong>' ); ?></p><?php // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped ?>

<?php if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) && $password_generated ) : ?>
	<?php /* translators: %s Auto generated password */ ?>
	<p><?php printf( esc_html__( 'Password: %s', 'woocommerce' ), '<strong>' . esc_html( $user_pass ) . '</strong>' ); ?></p>
<?php endif; ?>

<p><?php esc_html_e( 'We will be reviewing your account within 1 business day and when approved you will get final confirmation email.', 'woocommerce' ); ?></p>
<p><?php esc_html_e( 'You will be able to access your account area to to view orders, change password, and more at:', 'woocommerce' ); ?> <a href="https://aromen.be/my-account/">My account</a></p>

<p>
<?php esc_html_e( 'Best regards', 'woocommerce' ); ?>,
<br/>
<?php esc_html_e( 'Martijn Vanhoorelbeke & the Aromen Team', 'woocommerce' ); ?>
</p>
<?php
do_action( 'woocommerce_email_footer', $email );
