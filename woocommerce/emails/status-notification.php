<?php
/**
 * Customer approvation notification
 * @author  Woocommerce B2B
 * @version 1.0.0
 */
?>

<?php if ( ! defined( 'ABSPATH' ) ) { exit; } ?>

<?php $user = get_userdata( get_current_user_id() ); ?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php printf( __( 'Dear %s,', 'woocommerce-b2b' ), $user->first_name ); ?></p>
<p>
	<?php _e( 'Your account has been approved!', 'woocommerce-b2b' ); ?>
	<br />
	<a href="<?php echo get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ); ?>"><?php _e( 'Login here.', 'woocommerce-b2b' ); ?></a>
</p>
<p></p>



<p><?php _e( 'Tip: Our Quick-shop makes it fast and easy to order. Please check it out. ', 'woocommerce-b2b' ); ?><a href="https://aromen.be/quick-order/"><?php _e( 'Quick Order', 'woocommerce-b2b' ); ?></a></p>
<p>
<?php esc_html_e( 'Best regards', 'woocommerce' ); ?>,
<br/>
<?php esc_html_e( 'Martijn Vanhoorelbeke & the Aromen Team', 'woocommerce' ); ?>
</p>

<?php do_action( 'woocommerce_email_footer', $email ); ?>