<?php
/**
 * Email Footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
															</div>
														</td>
													</tr>
												</table>
												<!-- End Content -->
											</td>
										</tr>
									</table>
									<!-- End Body -->
								</td>
							</tr>
							<tr>
								<td align="center" valign="top">
									<!-- Footer -->
									<table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">
										<tr>
											<td valign="top">
												<table border="0" cellpadding="10" cellspacing="0" width="100%">
													<tr>
														<td colspan="2" valign="middle" id="credit">
															<?php echo wpautop( wp_kses_post( wptexturize( apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) ) ) ) ); ?>
														</td>
													</tr>
													
												</table>
											</td>
										</tr>
									</table>
									<!-- End Footer -->
								</td>
							</tr>
							<tr>
								<td valign="middle">
									<table border="0" width="600" cellpadding="10" style="padding: 20px;">
										<tr>
											<td style="padding-left: 10px; padding-right: 10px;" valign="middle">
												<img src="https://aromen.be/wp-content/uploads/2021/02/logo_new.jpg" alt="footer" width="136" height="21" style="margin-right:0px!important"/>
											</td>
											<td style="padding-left: 10px;" valign="middle">
												<h3 style="font-size: 10px; color: #164F2C">
													<strong>Aromen BVBA </strong>
												</h3>
												<p style="font-size: 10px;">
													Drieslaan 58 8560 Gullegem Belgium
												</p>
											</td>
											<td style="padding-left: 10px;" valign="middle">
												<h3 style="font-size: 10px; color: #164F2C">
													<strong>BTW/VAT</strong>
												</h3>
												<p style="font-size: 10px;">
													BE0632.475.632 IBAN BE40363149023463 BIC BBRUBEBB
												</p>
											</td>
											<td style="padding-left: 10px; padding-right: 10px;" valign="middle">
												<img src="https://aromen.be/wp-content/uploads/2021/02/loyly.png" alt="footer" width="86" height="101" style="margin-right:0px!important"/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
