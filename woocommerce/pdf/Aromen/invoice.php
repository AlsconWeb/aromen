<?php do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); ?>

<table class="head container">
	<tr>
		<td class="header">
			<img src="/home/247810.cloudwaysapps.com/gkeqvtkhez/public_html/wp-content/uploads/2020/06/email-header.jpg">
		</td>
	</tr>
</table>

<h1 class="document-type-label">
	<?php if( $this->has_header_logo() ) echo $this->get_title(); ?>
</h1>


<?php do_action( 'wpo_wcpdf_after_document_label', $this->type, $this->order ); ?>

<table class="order-data-addresses">
	<tr>
		<td class="order-data">
			<div>
				<table>
					<?php do_action( 'wpo_wcpdf_before_order_data', $this->type, $this->order ); ?>
					<tr class="page-number">
						<th>
							<?php _e( 'Page:', 'woocommerce-pdf-invoices-packing-slips' ); ?>
						</th>
						<td>1/1</td>
					</tr>
					<?php if ( isset($this->settings['display_date']) ) { ?>
					<!--<tr class="invoice-date">
						<th>
							<?php _e( 'Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?>
						</th>
						<td>
							<?php $this->invoice_date(); ?>
						</td>
					</tr>-->
					<?php } ?>
					<?php if ( isset($this->settings['display_number']) ) { ?>
					<!-- <tr class="invoice-number">
						<th>
							<?php /*_e( 'Ref:', 'woocommerce-pdf-invoices-packing-slips' );*/ ?>
						</th>
						<td>
							<?php /*$this->invoice_number();*/ ?>
						</td>
					</tr> -->
					<?php } ?>

					<tr class="order-number">
						<th>
							<?php _e( 'Reference:', 'woocommerce-pdf-invoices-packing-slips' ); ?>
						</th>
						<td>
							<?php $this->invoice_number(); ?>
						</td>
					</tr>
					<!--<tr class="order-date">
						<th>
							<?php _e( 'Order Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?>
						</th>
						<td>
							<?php $this->order_date(); ?>
						</td>
					</tr>-->
					<tr class="invoice-date">
						<th>
							<?php _e( 'Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?>
						</th>
						<td>
							<?php $this->invoice_date(); ?>
						</td>
					</tr>

					<tr class="contact-name">

						<th>
							<?php // _e( 'Contact:', 'woocommerce-pdf-invoices-packing-slips' ); ?>
						</th>
						<td>
							<?php //echo get_user_meta( $this->order->customer_id, 'billing_first_name', false ); ?>
							
							<?php //echo get_user_meta( $this->order->customer_id, 'billing_last_name', true ); ?>
						</td>
					</tr>
					<tr class="contact-email">

						<th>
							<?php _e( 'Email:', 'woocommerce-pdf-invoices-packing-slips' ); ?>
						</th>
						<td>
							<?php $this->billing_email(); ?>
						</td>
					</tr>
					<?php do_action( 'wpo_wcpdf_after_order_data', $this->type, $this->order ); ?>
				</table>
			</div>
		</td>

		<td class="address billing-address">
			<div>
				<table>
					<tr class="contact-name">

						<td>
							<?php echo $this->order->get_billing_first_name(); ?> <?php echo $this->order->get_billing_last_name(); ?>
						</td>
					</tr>
					<tr class="contact-name">

						<td>
							<?php echo $this->order->get_billing_company();  ?>
							
							
						</td>
					</tr>
					
					<tr class="contact-name">

						<td>
							<?php //echo get_user_meta( $this->order->customer_id, 'billing_address_1', true ); ?>
							<?php echo $this->order->get_billing_address_1(); ?>
						</td>
					</tr>
					<tr class="contact-name">

						<td>
							<?php //echo get_user_meta( $this->order->customer_id, 'billing_postcode', true ); ?>
							<?php //echo get_user_meta( $this->order->customer_id, 'billing_city', true ); ?>
							<?php echo $this->order->get_billing_postcode(); ?>
							<?php echo $this->order->get_billing_city(); ?>

						</td>
					</tr>
					<tr class="contact-name">

						<td>
							<?php //echo get_user_meta( $this->order->customer_id, 'billing_country', true ); ?>
							<?php echo WC()->countries->countries[$this->order->get_billing_country()]; ?>

						</td>
					</tr>
					<tr class="contact-name">

						<td>
							<?php //echo $this->order->get_address_prop( 'eu_vat_number', 'billing', $context ); ?>
							<?php //echo $this->order->billing_vat(); ?>
							<?php if ( '' != ( $billing_eu_vat_number = get_post_meta( $order->get_id(), '_billing_eu_vat_number', true ) ) ) { echo esc_html( $billing_eu_vat_number ); } ?>


						</td>

					</tr>
				</table>
			</div>
		</td>


	</tr>
</table>

<?php do_action( 'wpo_wcpdf_before_order_details', $this->type, $this->order ); ?>
<h2 style="text-align:center;margin-bottom:30px;"><?php _e('Invoice', 'woocommerce-pdf-invoices-packing-slips' ); ?></h2>
<table class="order-details">
	<thead>
		<tr>
			<th class="code">
				<?php _e('Code', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>
			<th class="product">
				<?php _e('Product', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>
			<th class="hs-code">
				<?php _e('HS Code', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>
			<!--<th class="type">
				<?php _e('Type', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>-->
			<!--<th class="batch">
				<?php // _e('Batch', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>-->
			<th class="size">
				<?php _e('Size', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>
			<th class="Qty">
				<?php _e('Qty', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>
			<!-- <th class="Price"><?php _e('Price/Unit', 'woocommerce-pdf-invoices-packing-slips' ); ?></th> -->
			<th class="Vat-pro">
				<?php _e('VAT (%)', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>
			<th class="Price-exVAT">
				<?php _e('Price exVAT', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>
			<th class="VAT">
				<?php _e('VAT', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>

		</tr>
	</thead>
	<tbody>
		<?php $regular_price = 0; ?>
		<?php $items = $this->get_order_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) : ?>
		<?php 
			// $batch_numbers = wc_get_order_item_meta($item_id, '_wcpbn_data', true);
			// if( !empty($batch_numbers) ) {
			// 		$print_batch = '';
			// 		foreach( $batch_numbers as $batch_number ) {
			// 			$print_batch .= ''.$batch_number['batch_number'].'<br/>';
			// 		}
			// 		$print_batch .= '';
			// }

			// Get an instance of corresponding the WC_Product object
			// $s_product = $item->get_product();
			
			// $s_product_name = $s_product->get_name(); // Get the product name
		
			// $s_item_quantity = $item->get_quantity(); // Get the item quantity
		
			// $s_item_total = $item->get_total(); // Get the item line total
			// $tax_rates = WC_Tax::get_rates( $s_product->get_tax_class() );
			// if (!empty($tax_rates)) {
			// 	$tax_rate = reset($tax_rates);
				
			// }
			
		?>
		<?php $decoded = json_decode($item["item"]); ?>
		<?php 
			echo count($decoded->meta_data);
			for($i = 0; $i < count($decoded->meta_data) ; $i++){
				$meta_array[$decoded->meta_data[$i]->key] = $decoded->meta_data[$i]->value;
				
			}
			//$size = $meta_array['pa_size'];			
			//$batch_number = $meta_array['pa_batch-number'];
			$product = wc_get_product( $item['product_id'] );
			// $s_product_name = $s_product->get_name(); // Get the product name
		
			// $s_item_quantity = $item->get_quantity(); // Get the item quantity
		
			// $s_item_total = $item->get_total(); // Get the item line total
			$tax_rates = ($decoded->subtotal_tax != '0')?number_format($decoded->subtotal_tax/$decoded->subtotal*100, 0):0;
			if (!empty($tax_rates)) {
			 	$tax_rate = reset($tax_rates);
				
			 }
			//  if($billing_eu_vat_number){
			// 	$tax_rate['rate'] = 0;
			//  }
			$price_incl_tax =  wc_get_price_including_tax( $product );
			$price_excl_tax =  number_format($decoded->subtotal,2);//round($price_incl_tax /(1 + ($tax_rate['rate'] / 100)),2);
			$vat_value = ($decoded->subtotal_tax != '0')? ' ' . number_format($decoded->subtotal_tax, 2): '--';//$price_incl_tax - $price_excl_tax;
    		$price_incl_tax = number_format($price_incl_tax, 2, ",", ".");
			$price_excl_tax = number_format($price_excl_tax, 2, ",", ".");
			$variation = new WC_Product_Variation( $item['variation_id'] );
			
			$attributes = $variation->get_attributes();
			$type = 's';
			$product_sku = $product->get_sku();
			$quantity = $item['quantity'];
			if($product->get_type() == 'variable'){
			$pprice = str_replace('.',',',$variation->get_price());
			$type = 'v';
			$size = $attributes['pa_size'];
			} else {
				// $size_a = $product->list_attributes();
				$size_a = implode(', ', wc_get_product_terms( $item['product_id'], 'pa_size', array( 'fields' => 'names' ) ) );
				$size_a .= implode(', ', wc_get_product_terms( $item['product_id'], 'size', array( 'fields' => 'names' ) ) );
				$pprice = str_replace('.',',',$product->get_price());
				if (empty($size_a)){
				$size = '1pc';

				}else {
				$size = str_replace('-en','',$size_a);
				$size = str_replace('-de','',$size_a);
				$size = str_replace('-be','',$size_a);

				}
			}

			
		
		
		?>

		<?php /*$batch_number =  $decoded->meta_data[1]->key;*/ ?>
		<?php //$batch_number = $decoded->meta_data[1]->value  ; ?>
		<?php $order = wc_get_order( $decoded->order_id ); ?>
		<?php $currency = get_woocommerce_currency_symbol( $order->get_currency() ); ?>
		<?php $price_single = $currency . ' ' . number_format($decoded->subtotal/$item['quantity'],2); ?>
		<?php $vat_percent = ($decoded->subtotal_tax != '0')?number_format($decoded->subtotal_tax/$decoded->subtotal*100, 0) . ' (%)': '--'; ?>
		<?php $vat_percent = $tax_rates;?>
		<?php //$price_item_ex_vat = $currency . ' ' . number_format($decoded->subtotal,2); ?>
		<?php //$price_item_ex_vat = $currency . ' ' . wc_get_price_excluding_tax( $product ) . ' ' . wc_get_price_including_tax( $product ); ?>
		<?php //$vat_value = ($decoded->subtotal_tax != '0')?$currency . ' ' . number_format($decoded->subtotal_tax, 2): '--'; ?>
		<?php //$item_single_price = (get_post_meta($decoded->variation_id, '_price', true) != '0')? get_post_meta($decoded->variation_id, '_price', true):get_post_meta($decoded->product_id, '_price', true); ?>
		<?php //$item_price = $item_single_price * $item['quantity']; ?>
		<?php // $user_discount = ($item_single_price - $decoded->subtotal) /$item_single_price*100; ?>
		<?php // $regular_price += number_format($item_price + $vat_percent*$item_price/100,2); ?>
		<?php $product_cat = get_the_terms( $decoded->product_id, 'product_cat' )[0]->name; ?>

		<?php 
		 if (  $wcb2b_user_group = get_the_author_meta( 'wcb2b_group', $this->order->customer_id ) ) : 
		 $user_level = get_post( $wcb2b_user_group );		 
		?>
		<?php $user_discount = get_post_meta( $user_level->ID, 'wcb2b_group_discount', true ) ; ?>
		<?php endif; ?>
		<?php $order_total = $order->get_order_item_totals()['order_total']['value'];?>


		<?php //$size = $decoded->meta_data[0]->value; ?>
		<?php $product = wc_get_product( $decoded->product_id ); ?>
		<?php $hscode = get_field('hscode', $decoded->product_id); ?>
		<tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', $item_id, $this->type, $this->order, $item_id ); ?> product-line">
			<td class="code">
				<?php if( !empty( $item['sku'] ) ) : ?>
				<?php echo $item['sku']; ?>
				<?php endif; ?>
			</td>
			<td class="product"><span class="item-name">
					<?php echo $item['name'] ; ?></span></td>
			<td class="hs-code">
				<?php echo $hscode;  ?>
			</td>
			<!--<td class="type">
				<?php if(!$product->is_type( 'bundle' )) echo $product_cat;  ?>
			</td>-->
			<!--<td class="bath">
				<?php //echo $batch_number;  ?>
				<?php // echo $print_batch;  ?>
			</td>-->
			<td class="size">
			<?php $size = str_replace('-en','',$size);
				$size = str_replace('-de','',$size);
				$size = str_replace('-be','',$size); ?>
				<?php echo $size ;  ?>
			</td>
			<td class="Qty">
				<?php echo $item['quantity']; ?>
			</td>
			<!-- <td class="price_single"><?php if(!$product->is_type( 'bundle' ))echo $price_single; ?></td> -->
			<td class="VAT">
				<?php if('' ==  $billing_eu_vat_number) echo $vat_percent;?>
			</td>
			<td class="Price-exVAT">
				<?php if(!$product->is_type( 'bundle' )) echo $currency . $price_excl_tax; ?>
			</td>
			<td class="VAT">
				<?php if('' ==  $billing_eu_vat_number) {echo $currency . str_replace('.',',',$vat_value); } ?>
			</td>

		</tr>

		<?php endforeach; endif; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="6" class="info">&nbsp;</td>
			<td class="white-bg"></td>
		<tr class="order-infoo">
			<td colspan="10" class="info white-bg" valign="middle"><?php if('' !=  $billing_eu_vat_number && get_user_meta( $this->order->customer_id, 'billing_country', true ) != 'BE') {?><i><?php _e('Btw verlegd/artikel 51 §2, 1° WBTW - BTW te voldoen door
			medecontractant / BTW verlegd'); ?></i><?php } ?><br/>
					<?php if(get_user_meta( $this->order->customer_id, 'billing_country', true ) == 'NO' || get_user_meta( $this->order->customer_id, 'billing_country', true ) == 'SE'):?>
					<i>Origin of content: Belgium<br/>
The exporter of the products covered by this document declares that, except where otherwise clearly indicated, these products are of Belgian Preferential origin.</i>
		 <?php endif;?>
</td><!--
			<td colspan="2" class="text-center white-bg"><i><?php// _e('Total', 'woocommerce-pdf-invoices-packing-slips' ); ?> <br><?php // _e('Tot. + discount', 'woocommerce-pdf-invoices-packing-slips' ); ?></i></td>
			<td colspan="2" class="text-center dark-bg"><i>
					<?php// echo $currency  . number_format($order->get_subtotal()/( 1- $user_discount/100),2); ?></i><br /><i><b>
						<?php // echo $currency . number_format($order->get_subtotal(),2); ?></b></i></td>-->
		</tr>
	</tfoot>

</table>
<table class="summary">
	<tr>
		<td class="sum-totals" colspan="4">
			<table class="summ-totals">

				<?php foreach( $this->get_woocommerce_totals() as $key => $total ) : ?>
				<tr class="<?php echo $key; ?>">
					<td class="no-borders"></td>
					<th class="description">
						<?php echo $total['label']; ?>
					</th>
					<td class="price"><span class="totals-price">
							<?php echo $total['value']; ?></span></td>
				</tr>
				<?php endforeach; ?>

			</table>
		</td>
		<td colspan="1" class="sum-space">

		</td>
		<td colspan="2" class=""><!-- class was sum-discount -->
			<!--<table>
				<tr>
					<th>Discount</th>
				</tr>
				<tr>

					<td><?php //echo $user_discount ?>%</td>
				</tr>

			</table>-->

		</td>
		<td colspan="3" class="sum-total-to-pay">
			<table>
				<tr>
					<th><?php _e('Total', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
				</tr>
				<tr>

					<td><?php echo $order_total; ?></td>
				</tr>

			</table>

		</td>

	</tr>
</table>

<table>

	<tr class="no-borders">
		<td>
			<div class="customer-notes">
				<?php do_action( 'wpo_wcpdf_before_customer_notes', $this->type, $this->order ); ?>
				<?php if ( $this->get_shipping_notes() ) : ?>
				<h3>
					<?php _e( 'Customer Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?>
				</h3>
				<?php $this->shipping_notes(); ?>
				<?php endif; ?>
				<?php do_action( 'wpo_wcpdf_after_customer_notes', $this->type, $this->order ); ?>
			</div>
		</td>

	</tr>

</table>




<?php do_action( 'wpo_wcpdf_after_order_details', $this->type, $this->order ); ?>


<div id="footer">
<table class="signature">
	<tr>
		<td class="sign-left" colspan="7" valign="bottom"><p><br/><br/><br/><br/><br/><br/><br/>*BE-BIO-01 Certisys</p></td>
		<td class="sign-right" colspan="3">
			<p>Signature<br/><strong>Martijn Vanhoorelbeke</strong><br/>&nbsp;</p>
			<img src="/home/247810.cloudwaysapps.com/gkeqvtkhez/public_html/wp-content/uploads/2018/11/signature@2x.png" alt="Martijn Vanhoorelbeke Signature" width="111"/>
		</td>
	</tr>
</table>
<!--	<table class="footer-nts">
	<tr>
		<td><p><strong><?php echo $order_total; ?></strong> to be paid before <strong><?php echo date('d/m/Y',strtotime('+30 day',strtotime($this->order->date_created)));?></strong></p></td>
		
		<td><p>on bank account <strong>BE40363149023463</strong> with reference <strong><?php $this->invoice_number(); ?></strong></p></td>
		<tr><td colspan="3"><p>The client declares to know the terms and conditions and accepts them without reservation</p></td></tr>
	</tr>
</table>-->

	
</div><!-- #letter-footer -->

<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order ); ?>