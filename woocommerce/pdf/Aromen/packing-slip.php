<?php do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); ?>

<table class="head container">
	<tr>
		<td class="header">
			<img src="/home/247810.cloudwaysapps.com/gkeqvtkhez/public_html/wp-content/uploads/2021/06/email-header.jpg"> 

		</td>
	</tr>
</table>

<h1 class="document-type-label">
<?php if( $this->has_header_logo() ) echo $this->get_title(); ?>
</h1>

<?php do_action( 'wpo_wcpdf_after_document_label', $this->type, $this->order ); ?>

<table class="order-data-addresses">
	<tr>
		<td class="order-data package">
			<div>
				<table>
					<?php do_action( 'wpo_wcpdf_before_order_data', $this->type, $this->order ); ?>
					<tr class="page-number">
						<th>
							<?php _e( 'Page:', 'woocommerce-pdf-invoices-packing-slips' ); ?>
						</th>
						<td>1/1</td>
					</tr>
					<?php if ( isset($this->settings['display_date']) ) { ?>
					<tr class="invoice-date">
						<th>
							<?php _e( 'Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?>
						</th>
						<td>
							<?php $this->invoice_date(); ?>
						</td>
					</tr>
					<?php } ?>
					

					<tr class="order-number">
						<th>
							<?php _e( 'Reference:', 'woocommerce-pdf-invoices-packing-slips' ); ?>
						</th>
						<td>
							<?php $this->invoice_number(); ?>
						</td>
					</tr>
					<tr class="order-date">
						<th>
							<?php _e( 'Order Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?>
						</th>
						<td>
							<?php $this->order_date(); ?>
						</td>
					</tr>

					<tr class="contact-name">

						<th>
							<?php //_e( 'Contact:', 'woocommerce-pdf-invoices-packing-slips' ); ?>
						</th>
						<td>
						</td>
					</tr>
					<tr class="contact-email">

						<th>
							<?php _e( 'Email:', 'woocommerce-pdf-invoices-packing-slips' ); ?>
						</th>
						<td>
							<?php $this->billing_email(); ?>
						</td>
					</tr>
					<tr class="contact-phone">

						<th>
							<?php _e( 'Phone:', 'woocommerce-pdf-invoices-packing-slips' ); ?>
						</th>
						<td>
							<?php $this->billing_phone(); ?>
						</td>
					</tr>
					<?php do_action( 'wpo_wcpdf_after_order_data', $this->type, $this->order ); ?>
				</table>
			</div>
		</td>

		<td class="address billing-address package">
			<div>
				<table>
						<tr class="contact-name">

						<td>
						<?php echo $this->order->get_billing_first_name(); ?> <?php echo $this->order->get_billing_last_name(); ?>

						</td>
						</tr>
						<tr class="contact-name">

						<td>
						<?php echo $this->order->get_billing_company();  ?>
						</td>
						</tr>
						<tr class="contact-name">

						<td>
							<?php //echo get_user_meta( $this->order->customer_id, 'billing_address_1', true ); ?>
							<?php echo $this->order->get_billing_address_1(); ?>
						</td>
						</tr>
						<tr class="contact-name">

						<td>
							<?php //echo get_user_meta( $this->order->customer_id, 'billing_postcode', true ); ?>
							<?php //echo get_user_meta( $this->order->customer_id, 'billing_city', true ); ?>
							<?php echo $this->order->get_billing_postcode(); ?>
							<?php echo $this->order->get_billing_city(); ?>

						</td>
						</tr>
						<tr class="contact-name">

						<td>
							<?php //echo get_user_meta( $this->order->customer_id, 'billing_country', true ); ?>
							<?php echo WC()->countries->countries[$this->order->get_billing_country()]; ?>

						</td>
						</tr>
						<tr class="contact-name">

						<td>
						<?php if ( '' != ( $billing_eu_vat_number = get_post_meta( $order->get_id(), '_billing_eu_vat_number', true ) ) ) { echo esc_html( $billing_eu_vat_number ); } ?>
							<?php //echo $this->order->billing_vat(); ?>


						</td>

						</tr>
				</table>
			</div>
		</td>


	</tr>
</table>

<?php do_action( 'wpo_wcpdf_before_order_details', $this->type, $this->order ); ?>
<h2 style="text-align:center;margin-bottom:30px;">Packing Slip</h2>
<table class="order-details">
	<thead>
		<tr>
			<th class="code">
				<?php _e('Code', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>
			<th class="product">
				<?php _e('Product', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>
			
			<th class="type">
				<?php _e('Type', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>
			<th class="Qty">
				<?php _e('Qty', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>
			<th class="size">
				<?php _e('Size', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>
			<!--<th class="batch">
				<?php // _e('Batch', 'woocommerce-pdf-invoices-packing-slips' ); ?>
			</th>-->
			
			
			

		</tr>
	</thead>
	<tbody>
		<?php $regular_price = 0; ?>
		<?php $items = $this->get_order_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) : ?>
		<?php $decoded = json_decode($item["item"]); ?>
		<?php //$batch_number =  $decoded->meta_data[1]->key; ?>
		<?php //$batch_number = $decoded->meta_data[1]->value  ; ?>
		<?php $order = wc_get_order( $decoded->order_id ); ?>
		<?php $currency = get_woocommerce_currency_symbol( $order->get_currency() ); ?>
		<?php $price_single = $currency . ' ' . number_format($decoded->subtotal/$item['quantity'],2); ?>
		<?php $vat_percent = ($decoded->subtotal_tax != '0')?number_format($decoded->subtotal_tax/$decoded->subtotal*100, 0) . ' (%)': '--'; ?>
		<?php $price_item_ex_vat = $currency . ' ' . number_format($decoded->subtotal,2); ?>
		<?php $vat_value = ($decoded->subtotal_tax != '0')?$currency . ' ' . number_format($decoded->subtotal_tax, 2): '--'; ?>
		<?php $item_price = get_post_meta($decoded->variation_id, '_price', true) * $item['quantity']; ?>
		<?php $user_discount = (get_post_meta($decoded->variation_id, '_price', true) - $decoded->subtotal) /get_post_meta($decoded->variation_id, '_price', true)*100; ?>
		<?php $regular_price += number_format($item_price + $vat_percent*$item_price/100,2); ?>
		<?php $product_cat = get_the_terms( $decoded->product_id, 'product_cat' )[0]->name; ?>

		<?php 
		 if (  $wcb2b_user_group = get_the_author_meta( 'wcb2b_group', $this->order->customer_id ) ) : 
		 $user_level = get_post( $wcb2b_user_group );		 
		?>
		<?php $user_discount = get_post_meta( $user_level->ID, 'wcb2b_group_discount', true ) ; ?>
		<?php endif; ?>
		<?php $order_total = $order->get_order_item_totals()['order_total']['value'];?>


		<?php $size = $decoded->meta_data[0]->value; ?>
		<?php $hscode = get_field('hscode', $decoded->product_id); ?>
		<?php 
			
			for($i = 0; $i < count($decoded->meta_data) ; $i++){
				$meta_array[$decoded->meta_data[$i]->key] = $decoded->meta_data[$i]->value;
				
			}
			$size = $meta_array['pa_size'];			
			//$batch_number = $meta_array['pa_batch-number'];
			
					$product = wc_get_product( $item['product_id'] );
					$variation = new WC_Product_Variation( $item['variation_id'] );
					
					$attributes = $variation->get_attributes();
					$type = 's';
					$product_sku = $product->get_sku();
					$quantity = $item['quantity'];
					if($product->get_type() == 'variable'){
						$pprice = str_replace('.',',',$variation->get_price());
						$type = 'v';
						$size = $attributes['pa_size'];
					} else {
						// $size_a = $product->list_attributes();
						$size_a = implode(', ', wc_get_product_terms( $item['product_id'], 'pa_size', array( 'fields' => 'names' ) ) );
						$size_a .= implode(', ', wc_get_product_terms( $item['product_id'], 'size', array( 'fields' => 'names' ) ) );
						
						if (empty($size_a)){
						$size = '1pc';
		
						}else {
						$size = str_replace('-en','',$size_a);
						$size = str_replace('-de','',$size_a);
						$size = str_replace('-be','',$size_a);
		
						}
					}
			
					// $batch_numbers = wc_get_order_item_meta($item_id, '_wcpbn_data', true);
					// if( !empty($batch_numbers) ) {
					// 		$print_batch = '';
					// 		foreach( $batch_numbers as $batch_number ) {
					// 			$print_batch .= ''.$batch_number['batch_number'].'<br/>';
					// 		}
					// 		$print_batch .= '';
					// }
				
		
		
		?>
		<tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', $item_id, $this->type, $this->order, $item_id ); ?> product-line">
			<td class="code">
				<?php if( !empty( $item['sku'] ) ) : ?>
				<?php echo $item['sku']; ?>
				<?php endif; ?>
			</td>
			<td class="product"><span class="item-name">
					<?php echo $item['name']; ?></span></td>
			
			<td class="type">
				<?php echo $product_cat;  ?>
			</td>
			<td class="quantity">
				<?php echo $item['quantity']; ?>
			</td>
			<td class="size">
				<?php echo $size;  ?>
				
			</td>
			<!--<td class="bath">
				<?php //echo $print_batch;  ?>
			</td>-->
			
			
			

		</tr>

		<?php endforeach; endif; ?>
	</tbody>
	

</table>

<?php do_action( 'wpo_wcpdf_after_order_details', $this->type, $this->order ); ?>

<?php do_action( 'wpo_wcpdf_before_customer_notes', $this->type, $this->order ); ?>
<div class="customer-notes">
	<?php if ( $this->get_shipping_notes() ) : ?>
		<h3><?php _e( 'Customer Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
		<?php $this->shipping_notes(); ?>
	<?php endif; ?>
</div>
<?php do_action( 'wpo_wcpdf_after_customer_notes', $this->type, $this->order ); ?>

<?php if ( $this->get_footer() ): ?>
<div id="footer">
	<?php // $this->footer(); ?>
</div><!-- #letter-footer -->
<?php endif; ?>

<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order ); ?>