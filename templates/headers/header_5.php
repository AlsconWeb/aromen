<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'organic_beauty_template_header_5_theme_setup' ) ) {
	add_action( 'organic_beauty_action_before_init_theme', 'organic_beauty_template_header_5_theme_setup', 1 );
	function organic_beauty_template_header_5_theme_setup() {
		organic_beauty_add_template(array(
			'layout' => 'header_5',
			'mode'   => 'header',
			'title'  => esc_html__('Header 5', 'organic-beauty'),
			'icon'   => organic_beauty_get_file_url('templates/headers/images/5.jpg')
			));
	}
}

// Template output
if ( !function_exists( 'organic_beauty_template_header_5_output' ) ) {
	function organic_beauty_template_header_5_output($post_options, $post_data) {

		// WP custom header
		$header_css = '';
		if ($post_options['position'] != 'over') {
			$header_image = get_header_image();
			$header_css = $header_image!='' 
				? ' style="background-image: url('.esc_url($header_image).')"' 
				: '';
		}
		?>

		<div class="top_panel_fixed_wrap"></div>

		<header class="top_panel_wrap top_panel_style_5 scheme_<?php echo esc_attr($post_options['scheme']); ?>">
		<div class="top-info"><p class="top-text"><?php echo get_field('top_bar', 'options')['text'];?></p></div>
			<div class="top_panel_wrap_inner top_panel_inner_style_5 top_panel_position_<?php echo esc_attr(organic_beauty_get_custom_option('top_panel_position')); ?>">
			
			<?php if (organic_beauty_get_custom_option('show_top_panel_top')=='yes') { ?>
				<div class="top_panel_top">
					<div class="content_wrap clearfix">
						<?php
						organic_beauty_template_set_args('top-panel-top', array(
							'top_panel_top_components' => array('login', 'currency', 'bookmarks', 'socials')
						));
						get_template_part(organic_beauty_get_file_slug('templates/headers/_parts/top-panel-top.php'));
						?>
					</div>
				</div>
			<?php } ?>

				<div class="top_panel_middle" <?php organic_beauty_show_layout($header_css); ?>>
					<div class="contact_logo">
						<?php organic_beauty_show_logo(true, true); ?>
					</div>
					<div class="top_panel_bottom">
						<div class="content_wrap clearfix">
								<nav class="menu_main_nav_area menu_hover_fade">
									<?php
									$menu_main = organic_beauty_get_nav_menu('menu_main');
									if (empty($menu_main)) $menu_main = organic_beauty_get_nav_menu();
									organic_beauty_show_layout($menu_main);
									?>
								</nav>
								<?php
								// Woocommerce Cart
						if (function_exists('organic_beauty_exists_woocommerce') && organic_beauty_exists_woocommerce() && (organic_beauty_is_woocommerce_page() && organic_beauty_get_custom_option('show_cart')=='shop' || organic_beauty_get_custom_option('show_cart')=='always') && !(is_checkout() || is_cart() || defined('WOOCOMMERCE_CHECKOUT') || defined('WOOCOMMERCE_CART'))) {
							?><div class="cart-container"><div class="cart-icon-new"><svg id="Warstwa_1" data-name="Warstwa 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 29.93 32.78"><defs><style>.cls-1,.cls-2{fill:none;}.cls-2{clip-rule:evenodd;}.cls-3{clip-path:url(#clip-path);}.cls-4{clip-path:url(#clip-path-2);}.cls-5{fill:#fff;}.cls-6{clip-path:url(#clip-path-3);}.cls-7{clip-path:url(#clip-path-5);}</style><clipPath id="clip-path" transform="translate(5 5)"><path class="cls-1" d="M16.37,20.64H3.56a1.42,1.42,0,0,1-1.42-1.42V7.83H4.27V9.61a1.07,1.07,0,0,0,2.14,0V7.83h7.11V9.61a1.07,1.07,0,0,0,2.14,0V7.83h2.13V19.22A1.43,1.43,0,0,1,16.37,20.64ZM13.52,5.69H6.41a3.56,3.56,0,0,1,7.11,0Zm2.85,17.09a3.56,3.56,0,0,0,3.56-3.56V5.69H15.66a5.7,5.7,0,0,0-11.39,0H0V19.22a3.56,3.56,0,0,0,3.56,3.56Z"/></clipPath><clipPath id="clip-path-2" transform="translate(5 5)"><rect class="cls-1" x="-1163" y="-237" width="1440" height="3218"/></clipPath><clipPath id="clip-path-3" transform="translate(5 5)"><path class="cls-2" d="M10.85,15.38a1.76,1.76,0,0,1-1.33,1.91c-.37,0-.69-.22-.69-.86,0-.92.46-1.74,2-2.11Zm3,2.45c-.24,0-.42-.21-.42-.64V13.58a2.57,2.57,0,0,0-2.54-2.75h0V14c-3.46.58-4.56,1.57-4.56,2.81a1.73,1.73,0,0,0,2,1.76,2.78,2.78,0,0,0,2.53-1.81h.07a1.74,1.74,0,0,0,1.87,1.75,2.1,2.1,0,0,0,1.82-.7l-.15-.18A.92.92,0,0,1,13.81,17.83Z"/></clipPath><clipPath id="clip-path-5" transform="translate(5 5)"><path class="cls-2" d="M8,13.88a1,1,0,0,0,1-1,2.23,2.23,0,0,1,1.23-2.11c-1.34,0-3.37.36-3.37,2A1,1,0,0,0,8,13.88"/></clipPath></defs><g class="cls-3"><g class="cls-4"><rect class="cls-5" width="29.93" height="32.78"/></g></g><g class="cls-6"><g class="cls-4"><rect class="cls-5" x="6.29" y="10.83" width="18.26" height="17.73"/></g></g><g class="cls-7"><g class="cls-4"><rect class="cls-5" x="6.86" y="10.79" width="13.37" height="13.09"/></g></g></svg></div><div class="cart-customlocation"><?php get_template_part(organic_beauty_get_file_slug('templates/headers/_parts/contact-info-cart.php')); ?></div></div><?php
						}
						?>
								<?php
								if (organic_beauty_get_custom_option('show_search')=='yes')
									organic_beauty_show_layout(organic_beauty_sc_search(array('state'=>"closed", "style"=>organic_beauty_get_theme_option('search_style'))));
								?>

						</div>
					</div>
				</div>
			</div>
		</header>

		<?php
		organic_beauty_storage_set('header_mobile', array(
				'login' => true,
				'socials' => true,
				'bookmarks' => false,
				'contact_address' => false,
				'contact_phone_email' => false,
				'woo_cart' => true,
				'search' => true
			)
		);
	}
}
?>