
// So if the sum of the 2 largest types our under or equal to 50 you take 3 types. Otherwise 2 types.

var app = new Vue({
    el: '#app',
    data: {
      message: 'Hello Vue!',
      currentQuestion: 1,
      answered: 0,
      screen:0,
      result:'',
      assets:'https://aromen.be/wp-content/themes/aromen/personality-test/',
      pointsEarned:{
          'blumig':0,
          'Citrus':0,
          'krautig':0,
          'waldig':0,
          'erdend':0,
          'würzig':0
      },
      errorMessage: '',
      appHeader: {
          title: 'Welche ätherischen Öle passen zu Ihrer Persönlichkeit? Machen Sie den Test!',
          subtitle: 'Vervollständigen Sie die Fragen. Geben Sie für jede Frage eine Antwort.',
          bg: 'header-bg-1.jpg',
          style: {
              background: '#093B25 url("https://aromen.be/wp-content/themes/aromen/personality-test/img/header-bg-1.jpg") no-repeat',
              backgroundSize: 'cover',
          }
      },
      questions:[
          { id: 1, text: 'Wie würden Sie sich selbst charakterisieren?', answers: [
              {id: 'A', text:'Ich bin immer fröhlich.', type: 'blumig', points: 2},
              {id: 'B', text:'Ich bin voller Energie.', type: 'Citrus', points: 2},
              {id: 'C', text:'Ich bin fürsorglich.', type: 'krautig', points: 2},
              {id: 'D', text:'Ich bin ein kreativer Denker.', type: 'waldig', points: 2},
              {id: 'E', text:'Ich bin prinzipientreu.', type: 'erdend', points: 2},
              {id: 'F', text:'Ich bin sozial.', type: 'würzig', points: 2},
            ],
            selected: 'not-selected',
            bg: 'img/q1h.jpg'
          },
          { id: 2, text: 'Wie können Sie am leichtesten entspannen?', answers: [
            {id: 'A', text:'Ich buche regelmäßig eine Massage oder etwas anderes Entspannendes.', type: 'blumig', points: 2},
            {id: 'B', text:'Ich genieße mein Hobby.', type: 'Citrus', points: 2},
            {id: 'C', text:'Ich lade einen Freund auf einen Drink ein.', type: 'krautig', points: 2},
            {id: 'D', text:'Ich lese ein Buch im Garten.', type: 'waldig', points: 2},
            {id: 'E', text:'Ich praktiziere Meditation oder Yoga.', type: 'erdend', points: 2},
            {id: 'F', text:'Ich gehe an einen ruhigen Ort, um zu entspannen.', type: 'würzig', points: 2},
            ],
            selected: 'not-selected',
            bg: 'img/q2h.jpg'
          },
          { id: 3, text: 'Andere sagen, dass ich...', answers: [
            {id: 'A', text:'...selbstbewusst im Leben stehe.', type: 'blumig', points: 2},
            {id: 'B', text:'...ein herzlicher und zuverlässiger Mensch bin.', type: 'Citrus', points: 2},
            {id: 'C', text:'...die hilfreichste Person bin, die sie kennen.', type: 'krautig', points: 2},
            {id: 'D', text:'...bin sehr sachkundig.', type: 'waldig', points: 2},
            {id: 'E', text:'...ihnen Seelenfrieden gebe.', type: 'erdend', points: 2},
            {id: 'F', text:'...ein guter Zuhörer bin.', type: 'würzig', points: 2},
            ],
            selected: 'not-selected',
            bg: 'img/q3h.jpg'
          },
          { id: 4, text: 'Ich kaufe gerne etwas Schönes, um einen persönlichen Raum noch gemütlicher und schöner zu gestalten.', answers: [
            {id: 'A', text:'Nie', type: 'blumig', points: 0},
            {id: 'B', text:'Selten', type: 'blumig', points: 0.5},
            {id: 'C', text:'Manchmal', type: 'blumig', points: 1},
            {id: 'D', text:'Oft', type: 'blumig', points: 1.5},
            {id: 'E', text:'Immer', type: 'blumig', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q4h.jpg'
          },
          { id: 5, text: 'Ich plane und strukturiere meine Aufgaben, bevor ich anfange.', answers: [
            {id: 'A', text:'Nie', type: 'Citrus', points: 0},
            {id: 'B', text:'Selten', type: 'Citrus', points: 0.5},
            {id: 'C', text:'Manchmal', type: 'Citrus', points: 1},
            {id: 'D', text:'Oft', type: 'Citrus', points: 1.5},
            {id: 'E', text:'Immer', type: 'Citrus', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q5h.jpg'
          },
          { id: 6, text: 'Ich engagiere mich regelmäßig als Sponsor oder Freiwilliger für wohltätige Zwecke.', answers: [
            {id: 'A', text:'Nie', type: 'krautig', points: 0},
            {id: 'B', text:'Selten', type: 'krautig', points: 0.5},
            {id: 'C', text:'Manchmal', type: 'krautig', points: 1},
            {id: 'D', text:'Oft', type: 'krautig', points: 1.5},
            {id: 'E', text:'Immer', type: 'krautig', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q6h.jpg'
          },
          { id: 7, text: 'Ich gehe häufig in der Natur spazieren.', answers: [
            {id: 'A', text:'Nie', type: 'waldig', points: 0},
            {id: 'B', text:'Einmal im Jahr, wenn ich im Urlaub bin', type: 'waldig', points: 0.5},
            {id: 'C', text:'Monatlich', type: 'waldig', points: 1},
            {id: 'D', text:'Jede Woche', type: 'waldig', points: 1.5},
            {id: 'E', text:'Wenn ich kann, jeden Tag!', type: 'waldig', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q7h.jpg'
          },
          { id: 8, text: 'Ich kaufe Bio-Lebensmittel und bevorzuge immer ökologische Produkte.', answers: [
            {id: 'A', text:'Nie', type: 'erdend', points: 0},
            {id: 'B', text:'Selten', type: 'erdend', points: 0.5},
            {id: 'C', text:'Manchmal', type: 'erdend', points: 1},
            {id: 'D', text:'Oft', type: 'erdend', points: 1.5},
            {id: 'E', text:'Immer', type: 'erdend', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q8h.jpg'
          },
          { id: 9, text: 'Ich fahre regelmäßig in den Urlaub oder plane eine Reise.', answers: [
            {id: 'A', text:'Nie', type: 'würzig', points: 0},
            {id: 'B', text:'Einmal alle 2 Jahre', type: 'würzig', points: 0.5},
            {id: 'C', text:'1 mal im Jahr', type: 'würzig', points: 1},
            {id: 'D', text:'2-3 mal im Jahr', type: 'würzig', points: 1.5},
            {id: 'E', text:'5+ mal im Jahr', type: 'würzig', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q9h.jpg'
          }     
      ],
      answers: [
          { type:'blumig', 
            texts : [
                'Mit einem hohen Anteil an ... sind Sie offensichtlich ein blumiger Typ! Die ätherischen Öle von Blumen wie Jasmin, Lavendel, Kamille und Ylang Ylang passen perfekt zu Ihnen!', 
                'Daneben sind Sie auch ein blumiger Typ mit ...%. Ätherische Öle von Jasmin, Lavendel, Kamille und Ylang Ylang sind eine ausgezeichnete Wahl für Sie!',
                'Sie sind ebenfalls ein blumiger Typ mit ...%. Ätherische Öle aus Jasmin, Lavendel, Kamille und Ylang Ylang sind hervorragend für Sie geeignet!'
                ],
                thumb:'img/a-floral.png',
                bg:'img/floral-header.jpg',
                color:'#B5358B'
            },
            {
                type: 'Citrus',
                texts: ['Mit einem hohen Anteil von ... sind Sie offensichtlich ein Zitrustyp! Die ätherischen Öle wie Orange, Limette, Zitrone, Bergamotte und Mandarine passen perfekt zu Ihnen!',
                'Daneben sind Sie auch ein Zitrustyp mit ...%. Die ätherischen Öle von Orange, Limette, Zitrone, Bergamotte und Mandarine sind eine ausgezeichnete Wahl für Sie!',
                'Sie sind ebenfalls ein Zitrustyp mit ...%. Ätherische Zitrusöle aus Orange, Limette, Zitrone, Bergamotte und Mandarine sind für Sie genau richtig!'
                ],
                thumb:'img/a-citrus.png',
                bg:'img/citrus-header.jpg',
                color:'#FFD400'
            },
            {
                type: 'krautig',
                texts: ['Mit einem hohen Anteil von ... sind Sie offensichtlich ein Kräutertyp! Die ätherischen Öle wie Basilikum, Eukalyptus, Majoran, Thymian, Rosmarin, Pfefferminze und Salbei passen perfekt zu Ihnen!',
                'Daneben sind Sie auch ein Kräutertyp mit ...%. Ätherische Öle aus Basilikum, Eukalyptus, Majoran, Thymian, Rosmarin, Pfefferminze und Salbei sind eine ausgezeichnete Wahl für Sie!',
                'Sie sind ebenfalls ein Kräutertyp mit ...%. Basilikum, Eukalyptus, Majoran, Thymian, Rosmarin, Pfefferminze und Salbei sind genau das Richtige für Sie!'
                ],
                thumb:'img/a-herbal.png',
                bg:'img/herbal-header.jpg',
                color:'#6C9B52'
            },
            {
                type: 'waldig',
                texts: ['The essential oils such as  sandalwood, cedarwood, swiss stone pine, balamfir and black spruce are a perfect fit for you!',
                'Essential oils of sandalwood, cedarwood, swiss stone pine, balamfir and black spruce are an excellent choice for you!',
                'Sandalwood, cedarwood, swiss stone pine, balamfir and black spruce are just great for you!'
                ],
                thumb:'img/a-woody.png',
                bg:'img/woody-header.jpg',
                color:'#602D00'
            },
            {
                type: 'erdend',
                texts: ['Mit einem hohen Anteil von ... sind Sie offensichtlich ein erdiger Typ! Die ätherischen Öle wie Patchouli, Myrrhe, Weihrauch, Vetiver, Elemi und Benzoe passen perfekt zu Ihnen!',
                'Daneben sind Sie auch ein erdiger Typ mit ...%. Ätherische Öle aus Patchouli, Myrrhe, Weihrauch, Vetiver, Elemi und Benzoe sind eine ausgezeichnete Wahl für Sie!',
                'Sie sind auch ein erdiger Typ mit ...%. Ätherische Öle von Patchouli, Myrrhe, Weihrauch, Vetiver, Elemi und Benzoe sind genau das Richtige für Sie!'
                ],
                thumb:'img/a-earthy.png',
                bg:'img/earthy-header.jpg',
                color:'#47392D'
            },
            
            {
                type: 'würzig',
                texts: ['Mit einem hohen Anteil von ... sind Sie offensichtlich ein würziger Typ! Die ätherischen Öle wie Ingwer, Sternanis, schwarzer Pfeffer, Zimt und Kurkuma passen perfekt zu Ihnen!',
                'Daneben sind Sie auch ein würziger Typ mit ...%. Ätherische Öle aus Ingwer, Sternanis, schwarzem Pfeffer, Zimt und Kurkuma sind eine ausgezeichnete Wahl für Sie!',
                'Sie sind ebenfalls ein würziger Typ mit ...%. Ingwer, Sternanis, schwarzer Pfeffer, Zimt und Kurkuma sind genau das Richtige für Sie!'
                ],
                thumb:'img/a-spicy.png',
                bg:'img/spicy-header.jpg',
                color:'#BA2E34'
            }
      ]
    },
    methods: {
        nextQuestion: function(){
            if(this.currentQuestion==9){
               // this.currentQuestion=1;
               this.calculateResults();
            } else {
                if(this.questions[this.currentQuestion-1].selected == 'not-selected'){
                    this.errorMessage = 'Bitte beantworten Sie zuerst die Frage.';
                } else {
                    this.errorMessage = '';

                    this.currentQuestion++;
                }
                
            }
            
        },
        prevQuestion: function(){
            if(this.currentQuestion==1){
                //this.currentQuestion=9;
            } else {
                this.currentQuestion--;
            }
            
        },
        calculateResults: function() {
            this.answered = 0;
            this.pointsEarned = {
                'blumig':0,
                'Citrus':0,
                'krautig':0,
                'waldig':0,
                'erdend':0,
                'würzig':0
            }
            for(const element of this.questions){
                if(element.selected == 'not-selected'){
                    this.errorMessage = 'Es gibt einige nicht beantwortete Fragen';
                } else {
                    this.answered++;
                    console.log(this.answered);
                    console.log(this.pointsEarned);
                    console.log(element.answers[element.selected].type);
                    console.log(element.answers[element.selected].points);
                    this.pointsEarned[element.answers[element.selected].type] += element.answers[element.selected].points;
                }
            }
            if(this.answered == 9){
                this.errorMessage = '';
                var sum = 0;
                for(const prop in this.pointsEarned){
                    sum += this.pointsEarned[prop];
                }
                console.log(sum);
                this.pointsEarned = {
                    'blumig':Math.round(this.pointsEarned['blumig']/sum*100),
                    'Citrus':Math.round(this.pointsEarned['Citrus']/sum*100),
                    'krautig':Math.round(this.pointsEarned['krautig']/sum*100),
                    'waldig':Math.round(this.pointsEarned['waldig']/sum*100),
                    'erdend':Math.round(this.pointsEarned['erdend']/sum*100),
                    'würzig':Math.round(this.pointsEarned['würzig']/sum*100)
                }
                console.log(this.pointsEarned);
                const sortable = Object.entries(this.pointsEarned)
                .sort(([,a],[,b]) => b-a)
                .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});

                console.log(sortable);
                var keyys = Object.keys(sortable);
                var newArray = [];
                newArray.push(this.answers.filter(element => element.type == keyys[0]));
                newArray.push(this.answers.filter(element => element.type ==keyys[1]));
                newArray.push(this.answers.filter(element => element.type ==keyys[2]));
                console.log(newArray); 
                
                this.appHeader.title = 'Sie sind ein <strong>' + keyys[0].toLowerCase() + ' typ!</strong>';
                this.appHeader.subtitle = 'Im Folgenden finden Sie eine detaillierte Übersicht über Ihre Ergebnisse.';
                this.appHeader.style= {
                    background: ''+newArray[0][0].color+' url("'+this.assets+newArray[0][0].bg+'") no-repeat',
                    backgroundSize: 'cover'
                }
                
                this.result = `
                    <div class="answer">
                        <div class="answer__img"><img src="${this.assets + newArray[0][0].thumb}"></div>
                        <div class="answer__text">
                            <h3 class="answer__title">Mit einem hohen Anteil an</h3>
                            <h2 class="answer__percentage">${this.pointsEarned[keyys[0]]}%</h2>
                            <p class="answer__content"> sie sind offensichtlich ein <span class="${keyys[0].toLowerCase()}-type">${keyys[0].toLowerCase()} typ!</span>  ${newArray[0][0].texts[0]}</p>
                        </div>

                    </div>
                `;
                if(this.pointsEarned[keyys[0]] < 50){
                    this.result += `
                    <div class="answer">
                        <div class="answer__img"><img src="${this.assets+newArray[1][0].thumb}"></div>
                        <div class="answer__text">
                            <h3 class="answer__title">Daneben sind Sie aber auch ein ${keyys[1].toLowerCase()} typ mit <strong>${this.pointsEarned[keyys[1]]}%</strong></h3>
                            
                            
                        </div>
                        <p class="answer__content">${newArray[1][0].texts[1]}</p>

                    </div>
                `;
                }
                
               
                if(this.pointsEarned[keyys[0]] + this.pointsEarned[keyys[1]] < 50){
                    this.result += `
                        <div class="answer">
                            <div class="answer__img"><img src="${this.assets+newArray[2][0].thumb}"></div>
                            <div class="answer__text">
                                <h3 class="answer__title">Sie sind auch ein ${keyys[2].toLowerCase()} type mit <strong>${this.pointsEarned[keyys[2]]}%</strong></h3>
                                
                            </div>
                            <p class="answer__content">${newArray[2][0].texts[2]}</p>
                        </div>
                    `;
                }
                
                this.screen=1;
            }
        },
        setClasses: function(index){
            if(index == this.questions[this.currentQuestion-1].selected ) {
                console.log(index, this.questions[this.currentQuestion-1].selected );
                return 'answers__item active';
            } else {
                return 'answers__item';
            }
        },
        handleSelected: function(item, event){
            console.log(item);
            console.log(event.currentTarget);
            this.questions[this.currentQuestion-1].selected = item;
            console.log(this.questions);

        },
        setPoints: function(e){
            console.log(this);
            console.log(this.currentQuestion);
            console.log(e.currentTarget);
            console.log(e.currentTarget.getElementsByClassName("answers_item_letter")[0].innerHTML);
            var letter = e.currentTarget.getElementsByClassName("answers_item_letter")[0].innerHTML;
            var result = this.questions[this.currentQuestion].answers.filter(obj => {
                return obj.id === letter
              });
              console.log(result[0].points);
              console.log(result[0].type);
            console.log(this.questions[this.currentQuestion].answers[0]);
            

        }
    }
  })