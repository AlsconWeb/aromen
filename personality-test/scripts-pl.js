
// So if the sum of the 2 largest types our under or equal to 50 you take 3 types. Otherwise 2 types.

var app = new Vue({
    el: '#app',
    data: {
      message: 'Hello Vue!',
      currentQuestion: 1,
      answered: 0,
      screen:0,
      result:'',
      assets:'https://aromen.be/wp-content/themes/aromen/personality-test/',
      pointsEarned:{
          'Kwiatowy':0,
          'Cytrusowy':0,
          'Ziołowy':0,
          'Drzewny':0,
          'Ziemny':0,
          'Korzenny':0
      },
      errorMessage: '',
      appHeader: {
          title: 'Które olejki pasują do Twojej osobowości? Rozwiąż test!',
          subtitle: 'Odpowiedz na pytania. Udziel 1 odpowiedzi na każde pytanie.',
          bg: 'header-bg-1.jpg',
          style: {
              background: '#093B25 url("https://aromen.be/wp-content/themes/aromen/personality-test/img/header-bg-1.jpg") no-repeat',
              backgroundSize: 'cover',
          }
      },
      questions:[
          { id: 1, text: 'Jak byś siebie scharakteryzował/a?', answers: [
              {id: 'A', text:'Jestem zawsze pogodny/a.', type: 'Kwiatowy', points: 2},
              {id: 'B', text:'Jestem pełen/na energii.', type: 'Cytrusowy', points: 2},
              {id: 'C', text:'Jestem opiekuńczy/a.', type: 'Ziołowy', points: 2},
              {id: 'D', text:'Jestem kreatywnym/ną myślicielem/ką.', type: 'Drzewny', points: 2},
              {id: 'E', text:'Jestem zasadniczy/a.', type: 'Ziemny', points: 2},
              {id: 'F', text:'Jestem towarzyski/a.', type: 'Korzenny', points: 2},
            ],
            selected: 'not-selected',
            bg: 'img/q1h.jpg'
          },
          { id: 2, text: 'W jaki sposób najłatwiej Ci się zrelaksować?', answers: [
            {id: 'A', text:'Regularnie umawiam się na masaż lub coś relaksującego.', type: 'Kwiatowy', points: 2},
            {id: 'B', text:'Cieszę się moim hobby.', type: 'Cytrusowy', points: 2},
            {id: 'C', text:'Zapraszam przyjaciela na drinka.', type: 'Ziołowy', points: 2},
            {id: 'D', text:'Czytam książkę w ogrodzie.', type: 'Drzewny', points: 2},
            {id: 'E', text:'Ćwiczę medytację lub jogę.', type: 'Ziemny', points: 2},
            {id: 'F', text:'Znajduję ciche miejsce, aby się odprężyć.', type: 'Korzenny', points: 2},
            ],
            selected: 'not-selected',
            bg: 'img/q2h.jpg'
          },
          { id: 3, text: 'Inni mówią, że ja...', answers: [
            {id: 'A', text:'...jestem pewny/a siebie w życiu.', type: 'Kwiatowy', points: 2},
            {id: 'B', text:'...jestem ciepłą i godną zaufania osobą.', type: 'Cytrusowy', points: 2},
            {id: 'C', text:'...jestem najbardziej pomocną osobą, jaką znają.', type: 'Ziołowy', points: 2},
            {id: 'D', text:'...mam dużą wiedzę.', type: 'Drzewny', points: 2},
            {id: 'E', text:'...zapewniam im spokój ducha.', type: 'Ziemny', points: 2},
            {id: 'F', text:'...jestem dobrym słuchaczem/ką.', type: 'Korzenny', points: 2},
            ],
            selected: 'not-selected',
            bg: 'img/q3h.jpg'
          },
          { id: 4, text: 'Lubię kupować coś ładnego, aby uczynić osobistą przestrzeń jeszcze bardziej przytulną i piękniejszą.', answers: [
            {id: 'A', text:'Nigdy', type: 'Kwiatowy', points: 0},
            {id: 'B', text:'Rzadko', type: 'Kwiatowy', points: 0.5},
            {id: 'C', text:'Czasami', type: 'Kwiatowy', points: 1},
            {id: 'D', text:'Często', type: 'Kwiatowy', points: 1.5},
            {id: 'E', text:'Zawsze', type: 'Kwiatowy', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q4h.jpg'
          },
          { id: 5, text: 'Planuję i organizuję swoje zadania przed ich rozpoczęciem.', answers: [
            {id: 'A', text:'Nigdy', type: 'Cytrusowy', points: 0},
            {id: 'B', text:'Rzadko', type: 'Cytrusowy', points: 0.5},
            {id: 'C', text:'Czasami', type: 'Cytrusowy', points: 1},
            {id: 'D', text:'Często', type: 'Cytrusowy', points: 1.5},
            {id: 'E', text:'Zawsze', type: 'Cytrusowy', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q5h.jpg'
          },
          { id: 6, text: 'Regularnie wspieram organizacje charytatywne lub jestem ich wolontariuszem.', answers: [
            {id: 'A', text:'Nigdy', type: 'Ziołowy', points: 0},
            {id: 'B', text:'Rzadko', type: 'Ziołowy', points: 0.5},
            {id: 'C', text:'Czasami', type: 'Ziołowy', points: 1},
            {id: 'D', text:'Często', type: 'Ziołowy', points: 1.5},
            {id: 'E', text:'Zawsze', type: 'Ziołowy', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q6h.jpg'
          },
          { id: 7, text: 'Często chodzę na spacery na łono natury.', answers: [
            {id: 'A', text:'Nigdy', type: 'Drzewny', points: 0},
            {id: 'B', text:'Raz w roku, kiedy jestem na wakacjach', type: 'Drzewny', points: 0.5},
            {id: 'C', text:'Raz w miesiącu', type: 'Drzewny', points: 1},
            {id: 'D', text:'Co tydzień', type: 'Drzewny', points: 1.5},
            {id: 'E', text:'Jeśli mogę, to codziennie!', type: 'Drzewny', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q7h.jpg'
          },
          { id: 8, text: 'Kupuję bioorganiczną żywność i zawsze wybieram produkty ekologiczne.', answers: [
            {id: 'A', text:'Nigdy', type: 'Ziemny', points: 0},
            {id: 'B', text:'Rzadko', type: 'Ziemny', points: 0.5},
            {id: 'C', text:'Czasami', type: 'Ziemny', points: 1},
            {id: 'D', text:'Często', type: 'Ziemny', points: 1.5},
            {id: 'E', text:'Zawsze', type: 'Ziemny', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q8h.jpg'
          },
          { id: 9, text: 'Regularnie wyjeżdżam na wakacje lub planuję podróż.', answers: [
            {id: 'A', text:'Nigdy', type: 'Korzenny', points: 0},
            {id: 'B', text:'Raz na 2 lata', type: 'Korzenny', points: 0.5},
            {id: 'C', text:'1 raz w roku', type: 'Korzenny', points: 1},
            {id: 'D', text:'2 - 3 razy w roku', type: 'Korzenny', points: 1.5},
            {id: 'E', text:'5 i więcej razy w roku', type: 'Korzenny', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q9h.jpg'
          }     
      ],
      answers: [
          { type:'Kwiatowy', 
            texts : [
                'Z najwyższym udziałem procentowym odpowiedzi  ... jesteś oczywiście typem kwiatowym! Olejki eteryczne z kwiatów takich jak jaśmin, lawenda, rumianek i ylang ylang idealnie do Ciebie pasują!', 
                'Równolegle do tego jesteś również typem kwiatowym w ...%. Olejki eteryczne z jaśminu, lawendy, rumianku i ylang ylang są dla Ciebie doskonałym wyborem!',
                'Ty również jesteś typem kwiatowym w ...%. Kwiatowe olejki eteryczne takie jak jaśmin, lawenda, rumianek i ylang ylang są dla Ciebie po prostu świetne!'
                ],
                thumb:'img/a-floral.png',
                bg:'img/floral-header.jpg',
                color:'#B5358B'
            },
            {
                type: 'Cytrusowy',
                texts: ['Z najwyższym udziałem procentowym odpowiedzi ... jesteś oczywiście typem cytrusowym! Olejki eteryczne takie jak pomarańcza, limonka, cytryna, bergamotka i mandarynka idealnie do Ciebie pasują!',
                'Dodatkowo, jesteś również typem cytrusowym w ...%. Olejki eteryczne z pomarańczy, limonki, cytryny, bergamotki i mandarynki są dla Ciebie doskonałym wyborem!',
                'Ty również jesteś typem cytrusowym w ...%. Cytrusowe olejki eteryczne z pomarańczy, limonki, cytryny, bergamotki i mandarynki są dla Ciebie po prostu świetne!'
                ],
                thumb:'img/a-citrus.png',
                bg:'img/citrus-header.jpg',
                color:'#FFD400'
            },
            {
                type: 'Ziołowy',
                texts: ['Z najwyższym udziałem procentowym odpowiedzi ... jesteś oczywiście typem ziołowym! Olejki eteryczne takie jak bazylia, eukaliptus, majeranek, tymianek, rozmaryn, mięta pieprzowa i szałwia idealnie pasują do Ciebie!',
                'Dodatkowo, jesteś również typem ziołowym w ...%. Olejki eteryczne z bazylii, eukaliptusa, majeranku, tymianku, rozmarynu, mięty pieprzowej i szałwii są dla Ciebie doskonałym wyborem!',
                'Ty również jesteś typem ziołowym w ...%. Bazylia, eukaliptus, majeranek, tymianek, rozmaryn, mięta pieprzowa i szałwia są dla Ciebie po prostu świetne!'
                ],
                thumb:'img/a-herbal.png',
                bg:'img/herbal-header.jpg',
                color:'#6C9B52'
            },
            {
                type: 'Drzewny',
                texts: ['Z najwyższym udziałem procentowym odpowiedzi ... jesteś oczywiście typem drzewnym! Olejki eteryczne takie jak drzewo sandałowe, drzewo cedrowe, szwajcarska sosna kamienna, balamfir i czarny świerk są dla Ciebie idealne!',
                'Dodatkowo, jesteś również typem drzewnym w ...%. Olejki eteryczne z drzewa sandałowego, cedrowego, sosny szwajcarskiej, balamfiru i czarnego świerku są dla Ciebie doskonałym wyborem!',
                'Ty również jesteś typem drzewnym w ...%. drzewo sandałowe, cedrowe, swiss stone pine, balamfir i czarny świerk są po prostu świetne dla Ciebie!'
                ],
                thumb:'img/a-woody.png',
                bg:'img/woody-header.jpg',
                color:'#602D00'
            },
            {
                type: 'Ziemny',
                texts: ['Z najwyższym udziałem procentowym odpowiedzi ... jesteś oczywiście typem ziemnym! Olejki eteryczne takie jak paczula, mirra, kadzidło, wetiwer, elemi i benzoin są dla Ciebie idealne!',
                'Dodatkowo, jesteś również typem ziemnym w ...%. Olejki eteryczne z paczuli, mirry, kadzidła, wetiweru, elemi i benzoinu są dla Ciebie doskonałym wyborem!',
                'Jesteś również typem ziemnym w ...%. olejki eteryczne paczuli, mirry, kadzidła, wetiweru, elemi i benzoinu są po prostu świetne dla Ciebie!'
                ],
                thumb:'img/a-earthy.png',
                bg:'img/earthy-header.jpg',
                color:'#47392D'
            },
            
            {
                type: 'Korzenny',
                texts: ['Z najwyższym udziałem procentowym odpowiedzi ... jesteś oczywiście typem korzennym! Olejki eteryczne takie jak imbir, anyż gwiaździsty, czarny pieprz, cynamon i kurkuma idealnie do Ciebie pasują!',
                'Dodatkowo, jesteś również typem korzennym w ...%. Olejki eteryczne z imbiru, anyżu gwiazdkowego, czarnego pieprzu, cynamonu i kurkumy są dla Ciebie doskonałym wyborem!',
                'Ty również jesteś typem korzennym w ...%. Imbir, anyż gwiazdkowy, czarny pieprz, cynamon i kurkuma są dla Ciebie po prostu świetne!'
                ],
                thumb:'img/a-spicy.png',
                bg:'img/spicy-header.jpg',
                color:'#BA2E34'
            }
      ]
    },
    methods: {
        nextQuestion: function(){
            if(this.currentQuestion==9){
               // this.currentQuestion=1;
               this.calculateResults();
            } else {
                if(this.questions[this.currentQuestion-1].selected == 'not-selected'){
                    this.errorMessage = 'Proszę, odpowiedz najpierw na pytanie.';
                } else {
                    this.errorMessage = '';

                    this.currentQuestion++;
                }
                
            }
            
        },
        prevQuestion: function(){
            if(this.currentQuestion==1){
                //this.currentQuestion=9;
            } else {
                this.currentQuestion--;
            }
            
        },
        calculateResults: function() {
            this.answered = 0;
            this.pointsEarned = {
                'Kwiatowy':0,
                'Cytrusowy':0,
                'Ziołowy':0,
                'Drzewny':0,
                'Ziemny':0,
                'Korzenny':0
            }
            for(const element of this.questions){
                if(element.selected == 'not-selected'){
                    this.errorMessage = 'Jest kilka pytań, na które nie ma odpowiedzi';
                } else {
                    this.answered++;
                    console.log(this.answered);
                    console.log(this.pointsEarned);
                    console.log(element.answers[element.selected].type);
                    console.log(element.answers[element.selected].points);
                    this.pointsEarned[element.answers[element.selected].type] += element.answers[element.selected].points;
                }
            }
            if(this.answered == 9){
                this.errorMessage = '';
                var sum = 0;
                for(const prop in this.pointsEarned){
                    sum += this.pointsEarned[prop];
                }
                console.log(sum);
                this.pointsEarned = {
                    'Kwiatowy':Math.round(this.pointsEarned['Kwiatowy']/sum*100),
                    'Cytrusowy':Math.round(this.pointsEarned['Cytrusowy']/sum*100),
                    'Ziołowy':Math.round(this.pointsEarned['Ziołowy']/sum*100),
                    'Drzewny':Math.round(this.pointsEarned['Drzewny']/sum*100),
                    'Ziemny':Math.round(this.pointsEarned['Ziemny']/sum*100),
                    'Korzenny':Math.round(this.pointsEarned['Korzenny']/sum*100)
                }
                console.log(this.pointsEarned);
                const sortable = Object.entries(this.pointsEarned)
                .sort(([,a],[,b]) => b-a)
                .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});

                console.log(sortable);
                var keyys = Object.keys(sortable);
                var newArray = [];
                newArray.push(this.answers.filter(element => element.type == keyys[0]));
                newArray.push(this.answers.filter(element => element.type ==keyys[1]));
                newArray.push(this.answers.filter(element => element.type ==keyys[2]));
                console.log(newArray); 
                
                this.appHeader.title = 'Jesteś <strong>' + keyys[0].toLowerCase() + ' typem!</strong>';
                this.appHeader.subtitle = 'Poniżej znajduje się szczegółowy przegląd Twoich wyników.';
                this.appHeader.style= {
                    background: ''+newArray[0][0].color+' url("'+this.assets+newArray[0][0].bg+'") no-repeat',
                    backgroundSize: 'cover'
                }
                
                this.result = `
                    <div class="answer">
                        <div class="answer__img"><img src="${this.assets + newArray[0][0].thumb}"></div>
                        <div class="answer__text">
                            <h3 class="answer__title">Z wysokim procentem</h3>
                            <h2 class="answer__percentage">${this.pointsEarned[keyys[0]]}%</h2>
                            <p class="answer__content"> jesteś oczywiście <span class="${keyys[0].toLowerCase()}-type">${keyys[0].toLowerCase()} typem!</span>  ${newArray[0][0].texts[0]}</p>
                        </div>

                    </div>
                `;
                if(this.pointsEarned[keyys[0]] < 50){
                    this.result += `
                    <div class="answer">
                        <div class="answer__img"><img src="${this.assets+newArray[1][0].thumb}"></div>
                        <div class="answer__text">
                            <h3 class="answer__title">Oprócz tego jesteś również ${keyys[1].toLowerCase()} typem z <strong>${this.pointsEarned[keyys[1]]}%</strong></h3>
                            
                            
                        </div>
                        <p class="answer__content">${newArray[1][0].texts[1]}</p>

                    </div>
                `;
                }
                
               
                if(this.pointsEarned[keyys[0]] + this.pointsEarned[keyys[1]] < 50){
                    this.result += `
                        <div class="answer">
                            <div class="answer__img"><img src="${this.assets+newArray[2][0].thumb}"></div>
                            <div class="answer__text">
                                <h3 class="answer__title">Jesteś również ${keyys[2].toLowerCase()} typem z <strong>${this.pointsEarned[keyys[2]]}%</strong></h3>
                                
                            </div>
                            <p class="answer__content">${newArray[2][0].texts[2]}</p>
                        </div>
                    `;
                }
                
                this.screen=1;
            }
        },
        setClasses: function(index){
            if(index == this.questions[this.currentQuestion-1].selected ) {
                console.log(index, this.questions[this.currentQuestion-1].selected );
                return 'answers__item active';
            } else {
                return 'answers__item';
            }
        },
        handleSelected: function(item, event){
            console.log(item);
            console.log(event.currentTarget);
            this.questions[this.currentQuestion-1].selected = item;
            console.log(this.questions);

        },
        setPoints: function(e){
            console.log(this);
            console.log(this.currentQuestion);
            console.log(e.currentTarget);
            console.log(e.currentTarget.getElementsByClassName("answers_item_letter")[0].innerHTML);
            var letter = e.currentTarget.getElementsByClassName("answers_item_letter")[0].innerHTML;
            var result = this.questions[this.currentQuestion].answers.filter(obj => {
                return obj.id === letter
              });
              console.log(result[0].points);
              console.log(result[0].type);
            console.log(this.questions[this.currentQuestion].answers[0]);
            

        }
    }
  })