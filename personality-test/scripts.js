
// So if the sum of the 2 largest types our under or equal to 50 you take 3 types. Otherwise 2 types.

var app = new Vue({
    el: '#app',
    data: {
      message: 'Hello Vue!',
      currentQuestion: 1,
      answered: 0,
      screen:0,
      result:'',
      assets:'https://aromen.be/wp-content/themes/aromen/personality-test/',
      pointsEarned:{
          'Floral':0,
          'Citrus':0,
          'Herbal':0,
          'Woody':0,
          'Earthy':0,
          'Spicy':0
      },
      errorMessage: '',
      appHeader: {
          title: 'Which personality type are you? Do the test!',
          subtitle: 'Complete the questions. Give 1 answer for each question.',
          bg: 'header-bg-1.jpg',
          style: {
              background: '#093B25 url("https://aromen.be/wp-content/themes/aromen/personality-test/img/header-bg-1.jpg") no-repeat',
              backgroundSize: 'cover',
          }
      },
      questions:[
          { id: 1, text: 'How would you characterise yourself?', answers: [
              {id: 'A', text:'I am always cheerful.', type: 'Floral', points: 2},
              {id: 'B', text:'I am full of energy.', type: 'Citrus', points: 2},
              {id: 'C', text:'I am caring.', type: 'Herbal', points: 2},
              {id: 'D', text:'I am a creative thinker.', type: 'Woody', points: 2},
              {id: 'E', text:'I am principled.', type: 'Earthy', points: 2},
              {id: 'F', text:'I am social.', type: 'Spicy', points: 2},
            ],
            selected: 'not-selected',
            bg: 'img/q1h.jpg'
          },
          { id: 2, text: 'How do you most easily relax?', answers: [
            {id: 'A', text:'I regularly book a massage or something else relaxing.', type: 'Floral', points: 2},
            {id: 'B', text:'I enjoy my hobby.', type: 'Citrus', points: 2},
            {id: 'C', text:'I invite a friend for a drink.', type: 'Herbal', points: 2},
            {id: 'D', text:'I read a book in the garden.', type: 'Woody', points: 2},
            {id: 'E', text:'I practise meditation or yoga.', type: 'Earthy', points: 2},
            {id: 'F', text:'I go to a quiet place to unwind.', type: 'Spicy', points: 2},
            ],
            selected: 'not-selected',
            bg: 'img/q2h.jpg'
          },
          { id: 3, text: 'Others say that I...', answers: [
            {id: 'A', text:'...am confident in life.', type: 'Floral', points: 2},
            {id: 'B', text:'...am a warm and reliable person.', type: 'Citrus', points: 2},
            {id: 'C', text:'...am the most helpful person they know.', type: 'Herbal', points: 2},
            {id: 'D', text:'...am very knowledgeable.', type: 'Woody', points: 2},
            {id: 'E', text:'...give them peace of mind.', type: 'Earthy', points: 2},
            {id: 'F', text:'...am a good listener.', type: 'Spicy', points: 2},
            ],
            selected: 'not-selected',
            bg: 'img/q3h.jpg'
          },
          { id: 4, text: 'I like to buy something nice to make a personal space even cozier and more beautiful.', answers: [
            {id: 'A', text:'Never', type: 'Floral', points: 0},
            {id: 'B', text:'Rarely', type: 'Floral', points: 0.5},
            {id: 'C', text:'Sometimes', type: 'Floral', points: 1},
            {id: 'D', text:'Often', type: 'Floral', points: 1.5},
            {id: 'E', text:'Always', type: 'Floral', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q4h.jpg'
          },
          { id: 5, text: 'I plan and structure my tasks before I start.', answers: [
            {id: 'A', text:'Never', type: 'Citrus', points: 0},
            {id: 'B', text:'Rarely', type: 'Citrus', points: 0.5},
            {id: 'C', text:'Sometimes', type: 'Citrus', points: 1},
            {id: 'D', text:'Often', type: 'Citrus', points: 1.5},
            {id: 'E', text:'Always', type: 'Citrus', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q5h.jpg'
          },
          { id: 6, text: 'I regularly sponsor or volunteer for charities.', answers: [
            {id: 'A', text:'Never', type: 'Herbal', points: 0},
            {id: 'B', text:'Rarely', type: 'Herbal', points: 0.5},
            {id: 'C', text:'Sometimes', type: 'Herbal', points: 1},
            {id: 'D', text:'Often', type: 'Herbal', points: 1.5},
            {id: 'E', text:'Always', type: 'Herbal', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q6h.jpg'
          },
          { id: 7, text: 'I frequently go for nature walks.', answers: [
            {id: 'A', text:'Never', type: 'Woody', points: 0},
            {id: 'B', text:'Once a year when I am on vacation', type: 'Woody', points: 0.5},
            {id: 'C', text:'Once a month', type: 'Woody', points: 1},
            {id: 'D', text:'Every week', type: 'Woody', points: 1.5},
            {id: 'E', text:'If I can every day!', type: 'Woody', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q7h.jpg'
          },
          { id: 8, text: 'I buy organic food and always prefer ecological products.', answers: [
            {id: 'A', text:'Never', type: 'Earthy', points: 0},
            {id: 'B', text:'Rarely', type: 'Earthy', points: 0.5},
            {id: 'C', text:'Sometimes', type: 'Earthy', points: 1},
            {id: 'D', text:'Often', type: 'Earthy', points: 1.5},
            {id: 'E', text:'Always', type: 'Earthy', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q8h.jpg'
          },
          { id: 9, text: 'I regularly go on holiday or plan a trip.', answers: [
            {id: 'A', text:'Never', type: 'Spicy', points: 0},
            {id: 'B', text:'Once every 2 years', type: 'Spicy', points: 0.5},
            {id: 'C', text:'1 time a year', type: 'Spicy', points: 1},
            {id: 'D', text:'2-3 times a year', type: 'Spicy', points: 1.5},
            {id: 'E', text:'5+ times a year', type: 'Spicy', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q9h.jpg'
          }     
      ],
      answers: [
          { type:'Floral', 
            texts : [
                'The essential oils of flowers such as jasmine, lavender, chamomile and ylang ylang are a perfect fit for you!', 
                'Essential oils of jasmine, lavender, chamomile and ylang ylang are an excellent choice for you!',
                'Floral essential oils like jasmine, lavender, chamomile and ylang ylang are just great for you!'
                ],
                thumb:'img/a-floral.png',
                bg:'img/floral-header.jpg',
                color:'#B5358B'
            },
            {
                type: 'Citrus',
                texts: ['The essential oils such as orange, lime, lemon, bergamot and mandarin are a perfect fit for you!',
                'Essential oils of orange, lime, lemon, bergamot and mandarin are an excellent choice for you!',
                'Citrus essential oils of orange, lime, lemon, bergamot and mandarin are just great for you!'
                ],
                thumb:'img/a-citrus.png',
                bg:'img/citrus-header.jpg',
                color:'#FFD400'
            },
            {
                type: 'Herbal',
                texts: ['The essential oils such as basil, eucalyptus, marjoram, thyme, rosemary, peppermint and sage are a perfect fit for you!',
                'Essential oils of basil, eucalyptus, marjoram, thyme, rosemary, peppermint and sage are an excellent choice for you!',
                'Basil, eucalyptus, marjoram, thyme, rosemary, peppermint and sage are just great for you!'
                ],
                thumb:'img/a-herbal.png',
                bg:'img/herbal-header.jpg',
                color:'#6C9B52'
            },
            {
                type: 'Woody',
                texts: ['The essential oils such as  sandalwood, cedarwood, swiss stone pine, balamfir and black spruce are a perfect fit for you!',
                'Essential oils of sandalwood, cedarwood, swiss stone pine, balamfir and black spruce are an excellent choice for you!',
                'Sandalwood, cedarwood, swiss stone pine, balamfir and black spruce are just great for you!'
                ],
                thumb:'img/a-woody.png',
                bg:'img/woody-header.jpg',
                color:'#602D00'
            },
            {
                type: 'Earthy',
                texts: ['The essential oils such as patchouli, myrrh, frankincense, vetiver, elemi and benzoin are a perfect fit for you!',
                'Essential oils of patchouli, myrrh, frankincense, vetiver, elemi and benzoin are an excellent choice for you!',
                'Patchouli, myrrh, frankincense, vetiver, elemi and benzoin are just great for you!'
                ],
                thumb:'img/a-earthy.png',
                bg:'img/earthy-header.jpg',
                color:'#47392D'
            },
            
            {
                type: 'Spicy',
                texts: ['The essential oils such as ginger, star anise, black pepper, cinnamon and turmeric are a perfect fit for you!',
                'Essential oils of ginger, star anise, black pepper, cinnamon and turmeric are an excellent choice for you!',
                'Ginger, star anise, black pepper, cinnamon and turmeric are just great for you!'
                ],
                thumb:'img/a-spicy.png',
                bg:'img/spicy-header.jpg',
                color:'#BA2E34'
            }
      ]
    },
    methods: {
        nextQuestion: function(){
            if(this.currentQuestion==9){
               // this.currentQuestion=1;
               this.calculateResults();
            } else {
                if(this.questions[this.currentQuestion-1].selected == 'not-selected'){
                    this.errorMessage = 'Please, answer the question first.';
                } else {
                    this.errorMessage = '';

                    this.currentQuestion++;
                }
                
            }
            
        },
        prevQuestion: function(){
            if(this.currentQuestion==1){
                //this.currentQuestion=9;
            } else {
                this.currentQuestion--;
            }
            
        },
        calculateResults: function() {
            this.answered = 0;
            this.pointsEarned = {
                'Floral':0,
                'Citrus':0,
                'Herbal':0,
                'Woody':0,
                'Earthy':0,
                'Spicy':0
            }
            for(const element of this.questions){
                if(element.selected == 'not-selected'){
                    this.errorMessage = 'There are some not anwered questions';
                } else {
                    this.answered++;
                    console.log(this.answered);
                    console.log(this.pointsEarned);
                    console.log(element.answers[element.selected].type);
                    console.log(element.answers[element.selected].points);
                    this.pointsEarned[element.answers[element.selected].type] += element.answers[element.selected].points;
                }
            }
            if(this.answered == 9){
                this.errorMessage = '';
                var sum = 0;
                for(const prop in this.pointsEarned){
                    sum += this.pointsEarned[prop];
                }
                console.log(sum);
                this.pointsEarned = {
                    'Floral':Math.round(this.pointsEarned['Floral']/sum*100),
                    'Citrus':Math.round(this.pointsEarned['Citrus']/sum*100),
                    'Herbal':Math.round(this.pointsEarned['Herbal']/sum*100),
                    'Woody':Math.round(this.pointsEarned['Woody']/sum*100),
                    'Earthy':Math.round(this.pointsEarned['Earthy']/sum*100),
                    'Spicy':Math.round(this.pointsEarned['Spicy']/sum*100)
                }
                console.log(this.pointsEarned);
                const sortable = Object.entries(this.pointsEarned)
                .sort(([,a],[,b]) => b-a)
                .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});

                console.log(sortable);
                var keyys = Object.keys(sortable);
                var newArray = [];
                newArray.push(this.answers.filter(element => element.type == keyys[0]));
                newArray.push(this.answers.filter(element => element.type ==keyys[1]));
                newArray.push(this.answers.filter(element => element.type ==keyys[2]));
                console.log(newArray); 
                
                this.appHeader.title = 'You are a <strong>' + keyys[0].toLowerCase() + ' type!</strong>';
                this.appHeader.subtitle = 'Check below for the detailed overview of your results.';
                this.appHeader.style= {
                    background: ''+newArray[0][0].color+' url("'+this.assets+newArray[0][0].bg+'") no-repeat',
                    backgroundSize: 'cover'
                }
                
                this.result = `
                    <div class="answer">
                        <div class="answer__img"><img src="${this.assets + newArray[0][0].thumb}"></div>
                        <div class="answer__text">
                            <h3 class="answer__title">With a high percentage of</h3>
                            <h2 class="answer__percentage">${this.pointsEarned[keyys[0]]}%</h2>
                            <p class="answer__content"> you are obviously a <span class="${keyys[0].toLowerCase()}-type">${keyys[0].toLowerCase()} type!</span>  ${newArray[0][0].texts[0]}</p>
                        </div>

                    </div>
                `;
                if(this.pointsEarned[keyys[0]] < 50){
                    this.result += `
                    <div class="answer">
                        <div class="answer__img"><img src="${this.assets+newArray[1][0].thumb}"></div>
                        <div class="answer__text">
                            <h3 class="answer__title">Alongside this, you are also a ${keyys[1].toLowerCase()} type with <strong>${this.pointsEarned[keyys[1]]}%</strong></h3>
                            
                            
                        </div>
                        <p class="answer__content">${newArray[1][0].texts[1]}</p>

                    </div>
                `;
                }
                
               
                if(this.pointsEarned[keyys[0]] + this.pointsEarned[keyys[1]] < 50){
                    this.result += `
                        <div class="answer">
                            <div class="answer__img"><img src="${this.assets+newArray[2][0].thumb}"></div>
                            <div class="answer__text">
                                <h3 class="answer__title">You are also a ${keyys[2].toLowerCase()} type with <strong>${this.pointsEarned[keyys[2]]}%</strong></h3>
                                
                            </div>
                            <p class="answer__content">${newArray[2][0].texts[2]}</p>
                        </div>
                    `;
                }
                
                this.screen=1;
            }
        },
        setClasses: function(index){
            if(index == this.questions[this.currentQuestion-1].selected ) {
                console.log(index, this.questions[this.currentQuestion-1].selected );
                return 'answers__item active';
            } else {
                return 'answers__item';
            }
        },
        handleSelected: function(item, event){
            console.log(item);
            console.log(event.currentTarget);
            this.questions[this.currentQuestion-1].selected = item;
            console.log(this.questions);

        },
        setPoints: function(e){
            console.log(this);
            console.log(this.currentQuestion);
            console.log(e.currentTarget);
            console.log(e.currentTarget.getElementsByClassName("answers_item_letter")[0].innerHTML);
            var letter = e.currentTarget.getElementsByClassName("answers_item_letter")[0].innerHTML;
            var result = this.questions[this.currentQuestion].answers.filter(obj => {
                return obj.id === letter
              });
              console.log(result[0].points);
              console.log(result[0].type);
            console.log(this.questions[this.currentQuestion].answers[0]);
            

        }
    }
  })