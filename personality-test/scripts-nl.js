
// So if the sum of the 2 largest types our under or equal to 50 you take 3 types. Otherwise 2 types.

var app = new Vue({
    el: '#app',
    data: {
      message: 'Hello Vue!',
      currentQuestion: 1,
      answered: 0,
      screen:0,
      result:'',
      assets:'https://aromen.be/wp-content/themes/aromen/personality-test/',
      pointsEarned:{
          'Floraal':0,
          'Citrus':0,
          'Kruidig':0,
          'Houtig':0,
          'Aards':0,
          'Pittig':0
      },
      errorMessage: '',
      appHeader: {
          title: 'Welk persoonlijkheidstype ben jij? Doe de test!',
          subtitle: 'Vul de vragen in. Geef telkens 1 antwoord per vraag.',
          bg: 'header-bg-1.jpg',
          style: {
              background: '#093B25 url("https://aromen.be/wp-content/themes/aromen/personality-test/img/header-bg-1.jpg") no-repeat',
              backgroundSize: 'cover',
          }
      },
      questions:[
          { id: 1, text: 'Hoe zou jij jezelf typeren?', answers: [
              {id: 'A', text:'Ik ben altijd vrolijk.', type: 'Floraal', points: 2},
              {id: 'B', text:'Ik zit vol energie.', type: 'Citrus', points: 2},
              {id: 'C', text:'Ik ben zorgzaam.', type: 'Kruidig', points: 2},
              {id: 'D', text:'Ik ben een creatieve denker.', type: 'Houtig', points: 2},
              {id: 'E', text:'Ik ben principieel.', type: 'Aards', points: 2},
              {id: 'F', text:'Ik ben sociaal.', type: 'Pittig', points: 2},
            ],
            selected: 'not-selected',
            bg: 'img/q1h.jpg'
          },
          { id: 2, text: 'Op welke manier ontspan jij het gemakkelijkst?', answers: [
            {id: 'A', text:'Ik boek regelmatig een massage of iets anders ontspannend.', type: 'Floraal', points: 2},
            {id: 'B', text:'Ik leef me uit in m’n hobby.', type: 'Citrus', points: 2},
            {id: 'C', text:'Ik nodig een vriend of vriendin uit om gezellig iets te komen drinken.', type: 'Kruidig', points: 2},
            {id: 'D', text:'Ik lees een boek in de tuin.', type: 'Houtig', points: 2},
            {id: 'E', text:'Ik doe aan meditatie of yoga.', type: 'Aards', points: 2},
            {id: 'F', text:'Ik zonder me even af op een rustige plek.', type: 'Pittig', points: 2},
            ],
            selected: 'not-selected',
            bg: 'img/q2h.jpg'
          },
          { id: 3, text: 'Anderen zeggen over mij dat ik...', answers: [
            {id: 'A', text:'...zelfverzekerd in het leven sta.', type: 'Floraal', points: 2},
            {id: 'B', text:'...een warm en betrouwbaar persoon ben.', type: 'Citrus', points: 2},
            {id: 'C', text:'...de meest hulpvaardige persoon ben die ze kennen.', type: 'Kruidig', points: 2},
            {id: 'D', text:'...veel kennis heb.', type: 'Houtig', points: 2},
            {id: 'E', text:'...hen rust geef.', type: 'Aards', points: 2},
            {id: 'F', text:'...heel goed kan luisteren.', type: 'Pittig', points: 2},
            ],
            selected: 'not-selected',
            bg: 'img/q3h.jpg'
          },
          { id: 4, text: 'Ik koop graag iets leuks om een persoonlijke ruimte nóg gezelliger en mooier te maken.', answers: [
            {id: 'A', text:'Nooit', type: 'Floraal', points: 0},
            {id: 'B', text:'Zelden', type: 'Floraal', points: 0.5},
            {id: 'C', text:'Soms', type: 'Floraal', points: 1},
            {id: 'D', text:'Vaak', type: 'Floraal', points: 1.5},
            {id: 'E', text:'Altijd', type: 'Floraal', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q4h.jpg'
          },
          { id: 5, text: 'Ik plan en structureer mijn taken voordat ik eraan begin.', answers: [
            {id: 'A', text:'Nooit', type: 'Citrus', points: 0},
            {id: 'B', text:'Zelden', type: 'Citrus', points: 0.5},
            {id: 'C', text:'Soms', type: 'Citrus', points: 1},
            {id: 'D', text:'Vaak', type: 'Citrus', points: 1.5},
            {id: 'E', text:'Altijd', type: 'Citrus', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q5h.jpg'
          },
          { id: 6, text: 'Ik sponsor of zet me regelmatig in als vrijwilliger voor goede doelen.', answers: [
            {id: 'A', text:'Nooit', type: 'Kruidig', points: 0},
            {id: 'B', text:'Zelden', type: 'Kruidig', points: 0.5},
            {id: 'C', text:'Soms', type: 'Kruidig', points: 1},
            {id: 'D', text:'Vaak', type: 'Kruidig', points: 1.5},
            {id: 'E', text:'Altijd', type: 'Kruidig', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q6h.jpg'
          },
          { id: 7, text: 'Ik ga regelmatig wandelen in de natuur.', answers: [
            {id: 'A', text:'Nooit', type: 'Houtig', points: 0},
            {id: 'B', text:'Een keer per jaar als ik op vakantie ben', type: 'Houtig', points: 0.5},
            {id: 'C', text:'Eens per maand', type: 'Houtig', points: 1},
            {id: 'D', text:'VaElke weekak', type: 'Houtig', points: 1.5},
            {id: 'E', text:'Als ik elke dag kan!', type: 'Houtig', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q7h.jpg'
          },
          { id: 8, text: 'Ik kies bewust voor bioproducten en geef steeds de voorkeur aan ecologische producten.', answers: [
            {id: 'A', text:'Nooit', type: 'Aards', points: 0},
            {id: 'B', text:'Zelden', type: 'Aards', points: 0.5},
            {id: 'C', text:'Soms', type: 'Aards', points: 1},
            {id: 'D', text:'Vaak', type: 'Aards', points: 1.5},
            {id: 'E', text:'Altijd', type: 'Aards', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q8h.jpg'
          },
          { id: 9, text: 'Ik ga regelmatig op vakantie of plan een tripje.', answers: [
            {id: 'A', text:'Nooit', type: 'Pittig', points: 0},
            {id: 'B', text:'Eens in de 2 jaar', type: 'Pittig', points: 0.5},
            {id: 'C', text:'1 keer per jaar', type: 'Pittig', points: 1},
            {id: 'D', text:'2-3 keer per jaar', type: 'Pittig', points: 1.5},
            {id: 'E', text:'5+ keer per jaar', type: 'Pittig', points: 2}
            ],
            selected: 'not-selected',
            bg: 'img/q9h.jpg'
          }     
      ],
      answers: [
          { type:'Floraal', 
            texts : [
                'Essentiële oliën van bloemen zoals jasmijn, lavendel, roomse kamille en ylang ylang zijn perfect voor jou!', 
                'Essentiële oliën van bloemen zoals jasmijn, lavendel, roomse kamille en ylang ylang matchen jou goed!',
                'Jasmijn, lavendel, roomse kamille en ylang ylang zijn uitermate geschikt voor jou!'
                ],
                thumb:'img/a-floral.png',
                bg:'img/floral-header.jpg',
                color:'#B5358B'
            },
            {
                type: 'Citrus',
                texts: ['Essentiële oliën zoals limoen, citroen, bergamot en mandarijn matchen jou goed!',
                'Essentiële oliën van limoen, citroen, bergamot en mandarijn zijn de perfecte match voor jou.',
                'Limoen, citroen, bergamot en mandarijn zijn uitermate geschikt voor jou!'
                ],
                thumb:'img/a-citrus.png',
                bg:'img/citrus-header.jpg',
                color:'#FFD400'
            },
            {
                type: 'Kruidig',
                texts: ['Essentiële oliën zoals basilicum, marjolein, tijm, rozemarijn, pepermunt en salie zijn perfect voor jou!',
                'Essentiële oliën van basilicum,  marjolein, tijm, rozemarijn, pepermunt en salie matchen jou goed!',
                'Basilicum, marjolein, tijm, rozemarijn, pepermunt en salie zijn uitermate geschikt voor jou!'
                ],
                thumb:'img/a-herbal.png',
                bg:'img/herbal-header.jpg',
                color:'#6C9B52'
            },
            {
                type: 'Houtig',
                texts: ['Essentiële oliën zoals sandelhout, kaneel, cederhout en kamfer zijn perfect voor jou!',
                'Essentiële oliën van sandelhout, kaneel, cederhout en kamfer matchen jou goed!',
                'Sandelhout, kaneel, cederhout en kamfer zijn uitermate geschikt voor jou!'
                ],
                thumb:'img/a-woody.png',
                bg:'img/woody-header.jpg',
                color:'#602D00'
            },
            {
                type: 'Aards',
                texts: ['Essentiële oliën zoals patchouli, mirre, wierook, vetiver, elemi en benzoë zijn perfect voor jou!',
                'Essentiële oliën van patchouli, mirre, wierook, vetiver, elemi en benzoë matchen jou goed!',
                'Patchouli, mirre, wierook, vetiver, elemi en benzoë zijn uitermate geschikt voor jou!'
                ],
                thumb:'img/a-earthy.png',
                bg:'img/earthy-header.jpg',
                color:'#47392D'
            },
            
            {
                type: 'Pittig',
                texts: ['Essentiële oliën van gember, steranijs, zwarte peper, kaneel en kurkuma zijn perfect voor jou!',
                'Essentiële oliën van gember, steranijs, zwarte peper, kaneel en kurkuma matchen jou goed!',
                'Gember, steranijs, zwarte peper, kaneel en kurkuma zijn zijn uitermate geschikt voor jou!'
                ],
                thumb:'img/a-spicy.png',
                bg:'img/spicy-header.jpg',
                color:'#BA2E34'
            }
      ]
    },
    methods: {
        nextQuestion: function(){
            if(this.currentQuestion==9){
               // this.currentQuestion=1;
               this.calculateResults();
            } else {
                if(this.questions[this.currentQuestion-1].selected == 'not-selected'){
                    this.errorMessage = 'Alsjeblieft, beantwoord eerst de vraag.';
                } else {
                    this.errorMessage = '';

                    this.currentQuestion++;
                }
                
            }
            
        },
        prevQuestion: function(){
            if(this.currentQuestion==1){
                //this.currentQuestion=9;
            } else {
                this.currentQuestion--;
            }
            
        },
        calculateResults: function() {
            this.answered = 0;
            this.pointsEarned = {
                'Floraal':0,
                'Citrus':0,
                'Kruidig':0,
                'Houtig':0,
                'Aards':0,
                'Pittig':0
            }
            for(const element of this.questions){
                if(element.selected == 'not-selected'){
                    this.errorMessage = 'Er zijn enkele onbeantwoorde vragen';
                } else {
                    this.answered++;
                    console.log(this.answered);
                    console.log(this.pointsEarned);
                    console.log(element.answers[element.selected].type);
                    console.log(element.answers[element.selected].points);
                    this.pointsEarned[element.answers[element.selected].type] += element.answers[element.selected].points;
                }
            }
            if(this.answered == 9){
                this.errorMessage = '';
                var sum = 0;
                for(const prop in this.pointsEarned){
                    sum += this.pointsEarned[prop];
                }
                console.log(sum);
                this.pointsEarned = {
                    'Floraal':Math.round(this.pointsEarned['Floraal']/sum*100),
                    'Citrus':Math.round(this.pointsEarned['Citrus']/sum*100),
                    'Kruidig':Math.round(this.pointsEarned['Kruidig']/sum*100),
                    'Houtig':Math.round(this.pointsEarned['Houtig']/sum*100),
                    'Aards':Math.round(this.pointsEarned['Aards']/sum*100),
                    'Pittig':Math.round(this.pointsEarned['Pittig']/sum*100)
                }
                console.log(this.pointsEarned);
                const sortable = Object.entries(this.pointsEarned)
                .sort(([,a],[,b]) => b-a)
                .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});

                console.log(sortable);
                var keyys = Object.keys(sortable);
                var newArray = [];
                newArray.push(this.answers.filter(element => element.type == keyys[0]));
                newArray.push(this.answers.filter(element => element.type ==keyys[1]));
                newArray.push(this.answers.filter(element => element.type ==keyys[2]));
                console.log(newArray); 
                
                this.appHeader.title = 'Je bent een <strong>' + keyys[0].toLowerCase() + ' type!</strong>';
                this.appHeader.subtitle = 'Kijk hieronder voor het gedetailleerde overzicht van uw resultaten.';
                this.appHeader.style= {
                    background: ''+newArray[0][0].color+' url("'+this.assets+newArray[0][0].bg+'") no-repeat',
                    backgroundSize: 'cover'
                }
                
                this.result = `
                    <div class="answer">
                        <div class="answer__img"><img src="${this.assets + newArray[0][0].thumb}"></div>
                        <div class="answer__text">
                            <h3 class="answer__title">Met maar liefst </h3>
                            <h2 class="answer__percentage">${this.pointsEarned[keyys[0]]}%</h2>
                            <p class="answer__content"> ben jij overduidelijk een <span class="${keyys[0].toLowerCase()}-type">${keyys[0].toLowerCase()} type!</span>  ${newArray[0][0].texts[0]}</p>
                        </div>

                    </div>
                `;
                if(this.pointsEarned[keyys[0]] < 50){
                this.result += `
                    <div class="answer">
                        <div class="answer__img"><img src="${this.assets+newArray[1][0].thumb}"></div>
                        <div class="answer__text">
                            <h3 class="answer__title">Daarnaast ben je ook met <strong>${this.pointsEarned[keyys[1]]}%</strong> een ${keyys[1].toLowerCase()} type</h3>
                            
                            
                        </div>
                        <p class="answer__content">${newArray[1][0].texts[1]}</p>

                    </div>
                `;
                }
                if(this.pointsEarned[keyys[0]] + this.pointsEarned[keyys[1]] < 50){
                    this.result += `
                        <div class="answer">
                            <div class="answer__img"><img src="${this.assets+newArray[2][0].thumb}"></div>
                            <div class="answer__text">
                                <h3 class="answer__title">Ook ben je een ${keyys[2].toLowerCase()} type met <strong>${this.pointsEarned[keyys[2]]}%</strong></h3>
                                
                            </div>
                            <p class="answer__content">${newArray[2][0].texts[2]}</p>
                        </div>
                    `;
                }
                
                this.screen=1;
            }
        },
        setClasses: function(index){
            if(index == this.questions[this.currentQuestion-1].selected ) {
                console.log(index, this.questions[this.currentQuestion-1].selected );
                return 'answers__item active';
            } else {
                return 'answers__item';
            }
        },
        handleSelected: function(item, event){
            console.log(item);
            console.log(event.currentTarget);
            this.questions[this.currentQuestion-1].selected = item;
            console.log(this.questions);

        },
        setPoints: function(e){
            console.log(this);
            console.log(this.currentQuestion);
            console.log(e.currentTarget);
            console.log(e.currentTarget.getElementsByClassName("answers_item_letter")[0].innerHTML);
            var letter = e.currentTarget.getElementsByClassName("answers_item_letter")[0].innerHTML;
            var result = this.questions[this.currentQuestion].answers.filter(obj => {
                return obj.id === letter
              });
              console.log(result[0].points);
              console.log(result[0].type);
            console.log(this.questions[this.currentQuestion].answers[0]);
            

        }
    }
  })