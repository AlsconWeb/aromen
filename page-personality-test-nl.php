	
<?php /* Template Name: Personality NL */ ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    

    <title>Personality Test - Aromen Aromatherapy</title>

	
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;700&display=swap" rel="stylesheet">
    
    <link rel="stylesheet" href="https://use.typekit.net/lga0mqp.css">
    <link rel="stylesheet" href="https://aromen.be/wp-content/themes/aromen/personality-test/styles.css?v=2" />
    <!--<link rel="icon" href="images/favicon.png" />-->
  </head>

  <body>
     <div id="app" class="main">
        
         
        
         <!-- header -->
         
         <div class="main__wrapper">
             <div class="header" v-bind:style="appHeader.style">
                 <div class="header_wrapper">
                    <img class="header__logo" src="https://aromen.be/wp-content/themes/aromen/personality-test/img/presonalitytestaromenlogo.svg" />  
                    <h1 class="header__title" v-html="appHeader.title"></h1>
                    <h3 class="header__subtitle" v-html="appHeader.subtitle"></h3>
                 </div>
                 
             </div>
         </div>
         <!-- header end -->
         <div class="content">
             <div class="content__wrapper">
                 <div class="screen-0"  v-if="screen ===0">
                    <div class="questions">
                        <div class="questions__item">
                            <h2 class="question__title">
                                <strong class="question__title__number">Vraag {{currentQuestion}}/9: </strong>{{questions[currentQuestion-1].text}}
                            </h2>
                            <div class="question__image__wrapper"><img v-bind:src="assets+questions[currentQuestion-1].bg" v-bind:alt="currentQuestion"></div>
                            <div class="answers">
                                <div v-bind:class="setClasses(index)" v-for="(answer, index) in questions[currentQuestion-1].answers" v-bind:key="answer.id" v-on:click="handleSelected(index, $event)">
                                    <div class="answers_item_letter">{{answer.id}}</div>
                                    <div class="answers_item_text">{{answer.text}}</div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="error-message" v-if="errorMessage">{{errorMessage}}</div>
                    <div class="navigation">
                        <div class="navigation__prev" v-on:click="prevQuestion"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-circle-left" class="svg-inline--fa fa-arrow-circle-left fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 504C119 504 8 393 8 256S119 8 256 8s248 111 248 248-111 248-248 248zm28.9-143.6L209.4 288H392c13.3 0 24-10.7 24-24v-16c0-13.3-10.7-24-24-24H209.4l75.5-72.4c9.7-9.3 9.9-24.8.4-34.3l-11-10.9c-9.4-9.4-24.6-9.4-33.9 0L107.7 239c-9.4 9.4-9.4 24.6 0 33.9l132.7 132.7c9.4 9.4 24.6 9.4 33.9 0l11-10.9c9.5-9.5 9.3-25-.4-34.3z"></path></svg> Vorige</div>
                        <div class="navigation__dots"></div>
                        <div class="navigation__next" v-on:click="nextQuestion">Volgende <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-circle-right" class="svg-inline--fa fa-arrow-circle-right fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 8c137 0 248 111 248 248S393 504 256 504 8 393 8 256 119 8 256 8zm-28.9 143.6l75.5 72.4H120c-13.3 0-24 10.7-24 24v16c0 13.3 10.7 24 24 24h182.6l-75.5 72.4c-9.7 9.3-9.9 24.8-.4 34.3l11 10.9c9.4 9.4 24.6 9.4 33.9 0L404.3 273c9.4-9.4 9.4-24.6 0-33.9L271.6 106.3c-9.4-9.4-24.6-9.4-33.9 0l-11 10.9c-9.5 9.6-9.3 25.1.4 34.4z"></path></svg></div>
                    </div>
                    <div class="footer__wrapper">
                        <img class="footer__logo" src="https://aromen.be/wp-content/themes/aromen/personality-test/img/personalitytestaromenlogogrey.svg" />
                    </div>
                </div>
                <div class="screen-1" v-if="screen===1">
                    <div class="results">     
                            <div class="result__html" v-html="result"></div>
                            <div class="social_media-share" style="display: none;">
                                <a href="https://www.facebook.com/aromenaromatherapy/">
                                <div class="socials fa-button">
                                    <img src="https://aromen.be/wp-content/themes/aromen/personality-test/img/instagram-square-brands.svg" /> 
                                    <p>Share on Facebook</p>   
                                </div>
                                </a>
                                <a href="https://www.instagram.com/aromen_aromatherapy/">
                                <div class="socials in-button">
                                    <img src="https://aromen.be/wp-content/themes/aromen/personality-test/img/facebook-square-brands.svg" /> 
                                    <p>Share on Instagram</p>   
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                 
             </div>
         </div>
     </div>
     <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script> 
    <script src="https://aromen.be/wp-content/themes/aromen/personality-test/scripts-nl.js?v=2"></script>
  </body>
</html>